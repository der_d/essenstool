<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 **/

return array(
	'maincss' => array(
		'//themes/css/style.css',
		'//themes/css/template.css',
		'//themes/css/modules.css'
	),
   
	'jquery' => array( 
    	'//themes/js/jquery/jquery_1.5.2.js',
		'//themes/js/jquery/base.js'
	)
);