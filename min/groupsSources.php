<?php

$base = realpath(dirname(__FILE__) . '/..');

$groupsSources = array(
	'maincss' => array(
		"{$base}/themes/css/style.css",
		"{$base}/themes/css/template.css",
		"{$base}/themes/css/modules.css"
	),
   
	'jquery' => array( 
    	"{$base}/themes/js/jquery/jquery_1.5.2.js",
		"{$base}/themes/js/jquery/base.js"
	)
);

unset($base);
