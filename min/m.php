<?php

require 'conf.php';
require 'groupsSources.php';
require 'lib/Minify.php';

if ($minifyCachePath) {
    Minify::setCache($minifyCachePath);
}

Minify::serve('Groups', array(
    'groups' => $groupsSources
    ,'setExpires' => time() + 86400 * 365
));
