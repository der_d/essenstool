<?php
class Config
{
    const SCRIPT_PATH = 'http://essenstool.ideaplexus.com/';
}

// Scriptpfad (URL)
$script_path 	= Config::SCRIPT_PATH;

// SEO URL-Rewrite 1/0 = Ja/Nein
$seoURL 		= 1;


// Absoluter Pfad auf Server
$dirPath		= "/";

// Entwicklungsumgebung
$development = 1;
$mysqldebug = 1;


// Aufschlag fuer Unkosten zwecks Rechnungsstellung
$unkostenpauschale = 0;
$unkostenbeitrag   = 0.05;