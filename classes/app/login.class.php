<?php
class login extends Application
{
	public $path = "";
	public $wartungsmodus = 0;

	public function __construct($path,$seoURL,$mail)
  	{
  		$this->path = $path;
    	Application::__construct($path,$seoURL,$mail);
  	}

    public static function isLoggedIn()
    {
        return (!empty($_SESSION['login']));
    }

  	public function geheZu()
  	{
  		$script = func::readURL();
  		$script = $script["aktion"];

        if (method_exists($this, $script)) {
            $this->$script();
        }
//        die('invalid "geheZu" method provided: "' . $script . '"');
  	}

	public function check($fromCookie = false)
	{
		$error = array();
        if (($fromCookie) && (isset($_COOKIE['mail']))) {
            $mail     = base64_decode($_COOKIE['mail']);
            $password = base64_decode($_COOKIE['passwordMd5']);
        } else {
            if(isset($_POST["mail"]) && $_POST["mail"]!="") {
                $mail   = $_POST["mail"];						// Kundenmail checken
            } else {
                $error[] = "";
            }
            if(isset($_POST["password"]) && $_POST["password"]!="") {		// Passwort checken
                $password = md5($_POST["password"]);
            } else {
                $error[] = "";
            }
        }

       	if(count($error) == 0) {
	        table::reset();												// vorherige DB-Abfrage leeren
	        table::$tablename = "eat_benutzer";        						// Verbindung mit Tabelle 'benutzer' herstellen
																		// Bedingung setzen
	        table::set_where("email", "=", "'$mail'");					// Where id='$kundenmail'
	        table::set_where(" AND password", "=", "'$password'");		// AND password='$pw';

	        table::get_array();											// Daten in Array speichern

	        if ( $this->wartungsmodus == 0 && count(table::$data) == 1 ) {
	        															// Wenn Datenbankabfrage einen Wert zurueckliefert
	            $var = table::$data[0];

	        	$_SESSION["login"] = $var["id"];					// setze Session auf Kundennummer

                if ($fromCookie) {
                    return true;
                }

                $expire = strtotime('+60 days');
                setcookie("mail", base64_encode($mail), $expire, '/');
                setcookie("passwordMd5", base64_encode($password), $expire, '/');

	            view::$data["seitentitel"] 	= "Login erfolgreich";			// Titel an View uebergeben
	        	view::$data["content"] 		= "views/login/login_ok.phtml";// Template an View uebergeben
	        	view::$data["redirect"] 	= func::writeURL('modul=kalender,kw='.date('W'));
																			// wieder zurueck zur vorherigen Seite leiten
	        	view::render();												// Template rendern
	        } elseif ($fromCookie) {
                return false;
            } elseif ( $this->wartungsmodus == 1 ) {
	        	view::$data["seitentitel"] = "Seite im Wartungsmodus";		// Titel an View uebergeben
	        	view::$data["message"] = "Die Seite befindet sich derzeit im Wartungsmodus. Aufgrund dessen kannst du dich derzeit nicht einloggen!<br /><br />Wir bitten um dein Verst&auml;ndniss.";
	        	view::$data["content"] = "views/system/message.phtml";	// Template an View uebergeben
	        	view::render();												// Template rendern
	        } else {													// Wenn kein benutzer gefunden
	            view::$data["seitentitel"] = "Login fehlgeschlagen";		// Titel an View uebergeben
	        	view::$data["content"] = "views/login/loginfehler.phtml";	// Template an View uebergeben
	        	view::render();												// Template rendern
	        }
        } elseif ($fromCookie) {
            return false;
       	} else {
			view::$data["seitentitel"] = "Login fehlgeschlagen";		// Titel an View uebergeben
	        view::$data["content"] = "views/login/loginfehler.phtml";	// Template an View uebergeben
	        view::render();												// Template rendern

       	}
	}

    private function logout()
    {
    	unset($_SESSION['login']);									// Session loeschen
        setcookie('mail', '', strtotime('-1 day'), '/');
        setcookie('passwordMd5', '', strtotime('-1 day'), '/');

        view::$data["seitentitel"] = "Logout erfolgreich";			// Titel an View uebergeben
        view::$data["content"] = "views/login/logout.phtml";		// Template an View uebergeben
        view::render();												// Template rendern
    }

    private function vergessen()
    {
    	view::$data["link"] = func::writeURL("modul=login,aktion=linkfordern");
    	view::$data["message"] = "Wenn du dein Passwort vergessen hast,<br /> dann gib bitte deine Daten ein und klicke auf <strong class='underlined'>Passwortlink anfordern</strong>.";
    	view::$data["seitentitel"] = "Passwort vergessen?";			// Titel an View uebergeben
        view::$data["content"] = "views/login/lostpassword.phtml";// Template an View uebergeben
        view::render();												// Template rendern
    }

    private function linkfordern()
    {
    	$login = LoginModell::linkfordernCheck();

    	if ( !isset($_POST["mail"]) OR $_POST["mail"] == "" ) {
        	view::$data["error"]["mail"] = "<span class='error'>E-Mail-Adresse eingeben!</span>";
   		}
    	if ( !isset($_POST["captcha"]) OR $_POST["captcha"] == "" ) {
                view::$data["error"]["captcha"] = "<span class='error'>Captcha eingeben!</span>";
   		}

   		if( @count(view::$data["error"]) == 0 ) {
   			if($_POST["captcha"] == $_SESSION["captcha"]) {
   				if( $login ) {
			    	$code = LoginModell::linkfordern();

			    	if( $code ) {

					    $title = "Passwortlink - Essentool";
						$to = $_POST["mail"];
						$message = "<html>
									<head>
									  <title>Passwortlink für das Essenstool</title>
									</head>
									<body>
										Unter folgendem Link kannst du nun dein Passwort ändern:<br />
										<a href='".func::writeURL("modul=login,aktion=pwaendern,code=$code")."' target='_blank'>Klick</a><br /><br />
										Falls dieser Link nicht funktioniert kopiere dir bitte folgende URL in deine Browserzeile:<br />
										".func::writeURL("modul=login,aktion=pwaendern,code=$code")."<br /><br />
									</body>";
						$header  ="From: Essenstool\n";
				   		$header .= "MIME-Version: 1.0\n";
		        		$header .= "Content-type: text/html; charset=UTF-8\n";
		        		$header .= "Content-Transfer-Encoding: 8BIT\r\n";

						if(mail($to,$title,$message,$header)) {
							view::$data["seitentitel"]			= "Passwortlink senden erfolgreich.";
							view::$data["message"]				= "Du hast erfolgreich einen &quot;Passwort &auml;ndern Link&quot; angefordert.<br /><br />Um dein Passwort zu &auml;ndern, klicke bitte auf den per E-Mail an dich gesendeten Link!";
						} else {
							view::$data["seitentitel"]			= "Passwortlink senden fehlgeschlagen.";
							view::$data["message"]				= "Beim Senden der Mail ist ein Fehler aufgetreten!<br /> Bitte kontaktieren den Webmaster, oder versuchen es in wenigen Minuten erneut!";
						}

			    	} else {
			    		view::$data["seitentitel"] = "Passwortlink senden fehlgeschlagen";			// Titel an View uebergeben
			    		view::$data["message"] = "Fehler!!!";
			    	}
			    	view::$data["content"] = "views/system/message.phtml";	// Template an View uebergeben
			        view::render();												// Template rendern
			    } else {
		    		view::$data["seitentitel"] = "Falsche Daten eingegeben";			// Titel an View uebergeben
		    		view::$data["message"] = "Fehler!!!";
		       		view::$data["content"] = "views/system/message.phtml";// Template an View uebergeben
		        	view::render();												// Template rendern
			    }
   			} else {
    			view::$data["error"]["captcha"] = "<span class='error'>Captcha falsch!</span>";			// Titel an View uebergeben
	    		self::vergessen();
	    		exit;
    		}
   		} else {
	    	self::vergessen();
	    	exit;
   		}
    }

    public function pwaendern()
    {
    	$script = func::readURL($this->scriptPath,parent::$seoURL);

    	view::$data["link"] = func::writeURL("modul=login,aktion=pwspeichern,code=".$script["code"]);
    	view::$data["message"] = "Gib bitte deine Daten und dein neues Passwort ein:";
    	view::$data["code"] = $script["code"];
    	view::$data["seitentitel"] = "Passwort &auml;ndern";				// Titel an View uebergeben
		view::$data["content"] = "views/login/changepw.phtml";			// Template an View uebergeben
		view::render();														// Template rendern
    }

    public function pwspeichern()
    {
    	$script = func::readURL($this->scriptPath,parent::$seoURL);

    	$login = LoginModell::linkfordernCheck("1");

    	if ( !isset($_POST["mail"]) OR $_POST["mail"] == "" ) {
        	view::$data["error"]["mail"] = "<span class='error'>E-Mail-Adresse eingeben!</span>";
   		}

    	if ( !isset($_POST["password"]) OR $_POST["password"] == "" ) {
                view::$data["error"]["password"] = "<span class='error'>Passwort eingeben!</span>";
   		}
    	if ( !isset($_POST["repeatpw"]) OR $_POST["repeatpw"] == "" ) {
                view::$data["error"]["repeatpw"] = "<span class='error'>Passwort wiederholung eingeben!</span>";
   		}
    	if ( @$_POST["password"]!=$_POST["repeatpw"] ) {
                view::$data["error"]["repeatpw"] = "<span class='error'>Passwort wiederholung inkorrekt!</span>";
   		}
    	if ( !isset($_POST["captcha"]) OR $_POST["captcha"] == "" ) {
                view::$data["error"]["captcha"] = "<span class='error'>Captcha eingeben!</span>";
   		}

   		if( @count(view::$data["error"]) == 0 ) {
   			if($_POST["captcha"] == $_SESSION["captcha"]) {
   				if( $login ) {
					loginModell::pwspeichern();
   					view::$data["seitentitel"]	= "Passwort erfolgreich ge&auml;ndert.";
					view::$data["message"]		= "Du hast erfolgreich dein Passwort ge&auml;ndert und kannst dich nun einloggen.";
			    	view::$data["content"] 		= "views/system/message.phtml";					// Template an View uebergeben
			        view::render();																// Template rendern
			    } else {
		    		view::$data["seitentitel"] = "Passwortlink senden fehlgeschlagen";			// Titel an View uebergeben
			    	view::$data["message"] = "Die von dir eingegebenen Daten sind inkorrekt!";
			    	view::$data["redirect"] = func::writeURL("modul=login,aktion=pwaendern,code=".$script["code"]);
			    	view::$data["content"] = "views/system/message.phtml";						// Template an View uebergeben
		        	view::render();																// Template rendern
			    }
   			} else {
    			view::$data["error"]["captcha"] = "<span class='error'>Captcha falsch!</span>";			// Titel an View uebergeben
	    		self::pwaendern();
	    		exit;
    		}
   		} else {
	    	self::pwaendern();
	    	exit;
   		}
    }

	private function registrieren()
    {
    	view::$data["message"] = "Gib bitte deine Daten ein:";
    	view::$data["seitentitel"] = "Registrieren";				// Titel an View uebergeben
        view::$data["content"] = "views/login/register.phtml";		// Template an View uebergeben
        view::render();												// Template rendern
    }


    public function speichern()
    {
    	$script = func::readURL($this->scriptPath,parent::$seoURL);

    	if ( !isset($_POST["mail"]) OR $_POST["mail"] == "" ) {
        	view::$data["error"]["mail"] = "<span class='error'>E-Mail-Adresse eingeben!</span>";
   		}
    	if ( !isset($_POST["password"]) OR $_POST["password"] == "" ) {
                view::$data["error"]["password"] = "<span class='error'>Passwort eingeben!</span>";
   		}
    	if ( !isset($_POST["repeatpw"]) OR $_POST["repeatpw"] == "" ) {
                view::$data["error"]["repeatpw"] = "<span class='error'>Passwort wiederholung eingeben!</span>";
   		}
    	if ( @$_POST["password"] != $_POST["repeatpw"] ) {
                view::$data["error"]["repeatpw"] = "<span class='error'>Passwort wiederholung inkorrekt!</span>";
   		}
   		if ( !isset($_POST["name"]) OR $_POST["name"] == "" ) {
                view::$data["error"]["name"] = "<span class='error'>Name eingeben!</span>";
   		}
    	if ( !isset($_POST["captcha"]) OR $_POST["captcha"] == "" ) {
                view::$data["error"]["captcha"] = "<span class='error'>Captcha eingeben!</span>";
   		}

   		if( @count(view::$data["error"]) == 0 ) {
   			if($_POST["captcha"] == $_SESSION["captcha"]) {
   				$data = LoginModell::userExists();
   				if( count($data) == 0 ) {
					loginModell::speichern();
					view::$data["seitentitel"]	= "Erfolgreich registriert.";
					view::$data["message"]		= "Du hast dich erfolgreich registriert und kannst dich nun einloggen.";
			    } else {
			    	view::$data["seitentitel"]	= "Bereits registriert!";
			    	view::$data["message"]		= "Du hast dich bereits registriert! <br /> <a href='".func::writeURL("modul=login,aktion=vergessen")."' title='Passwort vergessen?'>Passwort vergessen?</a>";
			    }
			    view::$data["content"] 			= "views/system/message.phtml";						// Template an View uebergeben
			    view::render();																		// Template rendern
   			} else {
    			view::$data["error"]["captcha"] = "<span class='error'>Captcha falsch!</span>";		// Titel an View uebergeben
	    		self::registrieren();
	    		exit;
    		}
   		} else {
	    	self::registrieren();
	    	exit;
   		}
    }
}
?>