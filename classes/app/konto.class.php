<?php 
class konto extends Application
{
	/**
	 * Konstruktor, der den Konstruktor seiner Elternklasse aufruft
	 * wichtig, da in dem Konstruktor der Elternklasse entsprechende Werte zu finden sind
	 * 
	 * @param string $script_path
	 * @param string $seoURL
	 */
	public function __construct($script_path,$seoURL,$mail) 
	{  		
    	Application::__construct($script_path,$seoURL,$mail);
  	}
  	
	/**
  	 * Funktion, die Methoden der Klasse aufruft
  	 */
  	public function geheZu() 
  	{
  		if(func::logged()) {
  			$this->anzeigen();
  		} else {
  			parent::verboten();
  		}
  	}
  	
  	/**
  	 * Zeigt alle Benutzerdaten an
  	 */
  	public function anzeigen() 
  	{
    	view::$data["seitentitel"]	= "Meine Daten";
  		view::$data["content"]		= "views/profile/overview.phtml";
  		view::render();
  	}
}
?>