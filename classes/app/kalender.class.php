<?php 
class kalender extends Application
{
	public function __construct($script_path,$seoURL,$mail) 
	{  		
    	Application::__construct($script_path,$seoURL,$mail);
  	}
	
	public function geheZu()
  	{
  		$script = func::readURL($this->scriptPath,parent::$seoURL);
  		$script = isset($script["aktion"]) ? $script["aktion"] : 'kalender';
		
  		$this->$script();
  	}
  	
  	public function kalender() 
  	{
		$startDay  = strtotime(date('Y-\\W'.parent::$kw));
		$endDay    = strtotime("+5 day",$startDay);
		$toDay	   = strtotime(date('d.m.Y'));
		$dateArray = array();

		$data = kalenderModell::data($startDay,$endDay);
	
		$bestellung = kalenderModell::bestellt($startDay,$endDay);
		
		$status = kalenderModell::status(parent::$kw);

		$bestellArray = array();
		foreach( $bestellung as $var ) {
			$bestellArray[$var['date']][$var['typ']] = array(
				'id'	=> $var['id'],
				'user'	=> $var['user'],
				'date'	=> $var['date'],
				'typ'	=> $var['typ']
			);
		}

        if ( func::$unkostenpauschale == 1 ) {
            $addPrice = func::$unkostenbeitrag;
        } else {
            $addPrice = 0;
        }


		foreach( $data as $var ) {

		    if ($var['typ'] == 8) {
		        $add = $var['price'];
		    } else {
		        $add = $var['price'] + $addPrice;
		    }

			view::$data['data'][$var['date']]['m'.$var['typ']] = array(
				'id'	=> $var['id'],
				'name'	=> $var['name'],
				'kcal'	=> $var['kcal'],
				'price'	=> number_format($add,2)
			);
		 	
			if( $bestellArray[$var['date']][$var['typ']] ) {
				view::$data['bestellt'][$var['date']]['m'.$var['typ']] = 1;
			}
			view::$data["status"][$var['date']] = ( ( $status[parent::$kw] == 0 ) || ( time() > $var['date'] ) || ( $var['id'] == '' ) ) ? ' disabled="disabled"' : '';
		}
		
  		view::$data['seitentitel']	= 'Kalender - Woche '.self::$kw;
		view::$data['content'] 		= 'views/kalender/index.phtml';
		view::render();
	}

	public function benutzer()
	{
		$startDay  = strtotime(date("d.m.Y"));
		
		$data = kalenderModell::menues($startDay);
		
		if( count($data) > 0 ) {
			view::$data['data'] = $data;
		} else {
			view::$data['data'] = false;
		}
		
		$data = kalenderModell::userOrders($startDay);
		
		foreach( $data as $var ) {
			view::$data['users'][$var['menu']][] = $var['name'];
		}
		
		view::$data['seitentitel'] = 'Bestell&uuml;bersicht - Benutzer - KW:'.parent::$kw;
		view::$data['content'] = 'views/kalender/list.phtml';
		view::render();
	}

}