<?php
class Database
{
    private $servername 	= 'localhost';
    private $benutzername 	= 'essenstool';
    private $kennwort 	    = 'essenstool';
    private $database 	    = 'essenstool';
    private $verbindung 	= null;

    /**
     * Datenbankverbindung herstellen
     */
	public function __construct()
    {
        $this->verbindung = mysql_connect($this->servername, $this->benutzername, $this->kennwort);

        if ($this->verbindung === false) {
            die(mysql_error());
        }

        mysql_select_db($this->database, $this->verbindung);
        mysql_query("SET NAMES 'utf8'");
		mysql_query("SET CHARACTER SET 'utf8'");
    }

    /**
     * Destruktor (Datenbankverbindung loeschen
     */
    public function __destruct()
    {
        mysql_close($this->verbindung);
    }

    public function __get($value)
    {
        return $this->$value;
    }

    public function __set($varname, $value)
    {
        $this->$varname = $value;
    }

    /**
     * Status der Verbindung ausgeben
     */
    public function status()
    {
        echo (!$this->verbindung) ? "keine Verbindung" : "Verbindung OK";
    }
}
?>
