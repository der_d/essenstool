<?php
class func
{
	// System
	static public $seoURL 		= "";
	static public $scriptPath 	= "";
	static public $mail		= "";
	static public $dirPath		= "";


	// Entwicklungsumgebung
	static public $development	= 0;
	static public $mysqldebug	= 0;


    // Unkostenaufrechnung
    static public $unkostenpauschale = 0;
    static public $unkostenbeitrag   = 0;

	/**
	 * Statische Methode, welche die URL (SEO optimiert) ausliest (Frontend)
	 * @return URL - Array
	 */
	static function readURL() {
		if(self::$seoURL==1) {
			$string = explode("?", $_SERVER['REQUEST_URI']);

			$array = explode("/",$string[0]);				// Explode die URI am '/'.
			unset($array[0]);			            		// Loesche 0tes und 1tes Feld aus array
            //unset($array[1]);
			$array = array_values($array);					// setze Keys zurueck (0,1,2,..)
		} else {
			$array = explode("?",$_SERVER['REQUEST_URI']);	// Explode die URI am '?'.
			$array = explode("=",$array['1']);				// Explode den 2ten teil der URI
			$array = implode("&",$array);					// Klebe elemente anhand '&' wieder zusammen
			$array = explode("&",$array);					// Explode nun gesammten String anhand '&'
		}
		$num = count($array);								// How many items in the array?

		$url_array = array();								// Init our new array

		for ($i = 1 ; $i < $num ; $i++) {					// Insert each element from the
			if(!preg_match('#/'.$array[$i].'/#',self::$scriptPath)) {
				$x = $i-1;
				$url_array[$array[$x]] = $array[$i];		// request URI into $url_array
				$i++;
			}
		}
		return $url_array;									// return our new shiny array
	}

	/**
	 * Statische Methode, welche die URL (SEO optimiert) ausliest (Backend)
	 * @return URL - Array
	 */
	static function readAdminURL() {
		if(self::$seoURL==1) {							// Wenn SEO-URL aktiviert
			$array = explode("/",$_SERVER['REQUEST_URI']);	// Explode die URI am '/'.
			unset($array['0'],$array['1']);		// Loesche 0tes und 1tes Feld aus array
			$array = array_values($array);					// setze Keys zurueck (0,1,2,..)
		} else {										// Sonst
			$array = explode("?",$_SERVER['REQUEST_URI']);	// Explode die URI am '?'.
			$array = explode("=",$array['1']);				// Explode den 2ten teil der URI
			$array = implode("&",$array);					// Klebe elemente anhand '&' wieder zusammen
			$array = explode("&",$array);					// Explode nun gesammten String anhand '&'
		}												// Ende
		$num = count($array);							// How many items in the array?

		$url_array = array();							// Init our new array

		for ($i = 1 ; $i < $num ; $i++) {				// Insert each element from the
			if(!preg_match('#/'.$array[$i].'/#',self::$scriptPath)) {
				$x = $i-1;
				$url_array[$array[$x]] = $array[$i];	// request URI into $url_array
				$i++;
			}
		}
		return $url_array;								// return our new shiny array
	}

	/**
	 * Statische Methode, welche die URL (SEO optimiert) erstellt (Frontend)
	 * @return URL
	 */
	static function writeURL($params) {
		$clear = " ";

		$blacklist = array(" ",",","=","&amp;",".","&","ö","ü","ä","Ä","Ö","Ü","ß");
		$whitelist = array("_","/","/","und","","und","oe","ue","ae","Ae","Oe","Ue","ss");

		$params = trim($params);


		if(self::$seoURL==1) {							// Wenn SEO-URL aktiviert
			$url = self::$scriptPath.str_replace($blacklist,$whitelist,$params);
		} else {										// Sonst
			$url = self::$scriptPath."index.php?".cb(str_replace($clear,"_",str_replace(",","&amp;",$params)));
		}												// Ende
		return $url;
	}

	/**
	 * Statische Methode, welche die URL (SEO optimiert) erstellt (Backend)
	 * @return URL
	 */
	static function writeAdminURL($params) {
		$clear = " ";
		if(self::$seoURL==1) {							// Wenn SEO-URL aktiviert
			$rep = array(",","=");
			$url = self::$scriptPath."admin/".str_replace($clear,"_",str_replace($rep,"/",$params));
		} else {										// Sonst
			$url = self::$scriptPath."admin/index.php?".str_replace($clear,"_",str_replace(",","&amp;",$params));
		}												// Ende
		return $url;
	}



	/**
	 * Validiert E-Mail - Adresse
	 * erlaubte Zeichen: Block1@Block2.Block3
	 *
	 * Block 1: a-z,A-Z,0-9 _ . -
	 * Block 2: a-z,A-Z,0-9,ä,ö,ü,Ä,Ö,Ü _ . -
	 * Block 3: a-z,A-Z,0-9 .
	 *
	 * @param string $mail
	 * @return true|false
	 */
	static public function validate_email($string) {
		$validate = '#^[a-zA-Z0-9öüä_\.-]+@[a-zA-Z0-9äöüÄÖÜ_\.-]+\.[a-zA-Z0-9\.]{2,5}+$#';

		if(preg_match($validate,$string)) {
			return true;
		}else{
			return false;
		}
	}


	/**
	 * Abfragen, ob Benutzer eingeloggt
	 */
	static function logged() {
		if(isset($_SESSION["login"]) && $_SESSION["login"]!==false) {
			return $_SESSION["login"];
		} else {
			return false;
		}
	}


	/**
	 * eingabe fuer Datenbank vorbereiten
	 * @return escapeter Wert
	 */
	static function escape($text) {
		return mysql_escape_string($text);
	}

	/**
	 * Benutzerdaten auslesen
	 * @return Vorname
	 */
	static function userdata($id="") {
		table::reset();
		table::$tablename = "eat_benutzer";

		$id = $id!="" ? $id : func::logged();

		table::set_where("id","=","'".$id."'");
		table::get_array();

		if( count(table::$data) > 0 ) {
			return array(
				"id" 		=> (table::$data[0]["id"]) 		? table::$data[0]["id"]		: "",
				"mail" 		=> (table::$data[0]["email"]) 	? table::$data[0]["email"]	: "",
				"rights"	=> (table::$data[0]["rights"]) 	? table::$data[0]["rights"]	: 0,
				"name"		=> (table::$data[0]["name"])	? table::$data[0]["name"]	: ""
			);
		} else {
			return false;
		}
	}

	/**
	 * Benutzerdaten anhand von ID auslesen
	 * @return Vorname
	 */
	static function user($id) {
		table::reset();
		table::$tablename = "eat_benutzer";
		table::set_where("id","=","'$id'");

		table::get_array();

		$array = array(
			"id" 	=> (table::$data[0]["id"]) 			? table::$data[0]["id"]		: "",
			"mail" 		=> (table::$data[0]["email"]) 	? table::$data[0]["email"]	: ""
		);
		return $array;
	}


	/**
	 * Abfragen, ob Benutzer im Admin eingeloggt
	 */
	static function adminLogged() {
		if(isset($_COOKIE["adminlogin"]) && $_COOKIE["adminlogin"]!==false) {
			$inhalt = $_COOKIE["adminlogin"];

			$admin = explode("_*_",$inhalt);
			return $admin[0];
		} else {
			return false;
		}
	}


	/**
	 * Menge mit Preis Multiplizieren
	 */
	static function pricePosition($menge,$preis) {
		$gesamt = $menge * $preis;
		return number_format($gesamt,2);
	}



	/*
	 * Adminrechte auslesen
	 * @param int $id
	 */
	static function adminRight($id,$right="")
	{
		$var = explode("#",$id);
		$id = $var[1];
		table::reset();												// vorherige DB-Abfrage leeren
        table::$tablename = "administratoren";             			// Verbindung mit Tabelle 'inhalt' herstellen
        table::set_where("id", "=", "'".$id."'");					// ID abfragen
        table::get_array();											// Daten als array zurueckgeben
		if( count(table::$data[0]) > 0 ) {							// Abfragen, ob Datensatz vorhanden
	        if( in_array($right,explode(":",table::$data[0]["rechte"])) OR table::$data[0]["superadmin"] == 1 ) {
	        															// Abfragen, ob Werte vorhanden
				return true;												// true zurueckgeben
	        } else {													// Sonst
				return false;												// false zurueckgeben
	        }															// Ende
		} else {													// Sonst
			return false;												// false zurueckgeben
		}															// Ende
	}

	/**
	 * Mail Template laden und mit Inhalt fuellen
	 */
	static function createMail($tplpath,$csspath,$title,$betreff,$content,$extra="")
	{
		eval ("\$mail = stripslashes(\"".addslashes(file_get_contents($tplpath."views/system/mail.phtml"))."\");");
		return $mail;
	}

	/**
	 * text kuerzen (gekuertzt - Zeichen)
	 * @param $text = zu kurzender Text
	 * @param $start = ab der wievielten Stelle soll der Text zurueckgegeben werden
	 * @param $end = wie viele Zeichen max. zurueckgeben
	 *
	 * @return gekuertzter Text
	 */
	public static function truncate($text,$max = 250){
		if( strlen($text) >= $max-1 ) {
	        return preg_replace('#[^ ]*$#', '', substr($text, 0, $max)). " ...";
	    } else {
	        return $text;
	    }
	}

	/**
	 * text kuerzen (gekuertzt - Zeichen)
	 * @param $text = zu kurzender Text
	 * @param $max = ab der wievielten Stelle soll der Text zurueckgegeben werden
	 *
	 * @return gekuertzter Text
	 */
	public static function simpleTruncate($text,$max = 250){
		if( strlen($text) >= $max-1 ) {
	        return substr($text, 0, $max). " ...";
	    } else {
	        return $text;
	    }
	}


	static function entities($string) {
		return html_entity_decode($string);
	}

	static function ampersandEncode($text) {
		return preg_replace("#& #","&amp; ",$text);
	}

	static function weekDay($date) {
		$array = array(
			'mon' 	=> 'Montag',
			'tue'	=> 'Dienstag',
			'wed'	=> 'Mittwoch',
			'thu'	=> 'Donnerstag',
			'fri'	=> 'Freitag',
			'sat'	=> 'Sonnabend',
			'sun'	=> 'Sonntag'
		);
		#echo date('D',$date);
		return $array[strtolower(date('D',$date))];
	}

}
?>
