<?php
function escapeRight($text){
	$bad = array('/& /','/\'/');
	$good = array('&amp; ','&#39;');
	
    return preg_replace($bad,$good,html_entity_decode($text,ENT_QUOTES,'UTF-8'));
}

/* oeffentliche Methoden und Attribute:

order_by($sortfeld)
    * Parameter string $sortfeld
    * setzt Sortierung der Daten
    * Mehrfachaufruf mouglich
    
set_where($fieldname, $condition, $value)
    * Parameter string $fieldname --> der Feldname der Tabelle
    * Parameter string $condition --> der Vergleichsoperator
    * Parameter string $value     --> der zu filternde Inhalt
    * setzt Filter in den Daten (Where-Klausel)
    
get_data()
    * führt Abfrage aus und füllt Datenarray $data
    
reset()
    * setzt $data, $sort, $where auf Grundzustand    
    
Attribut $data
    * beinhaltet immer aktuelle Ergebnisdaten als assotiatives Array     


*/

class table
{
	
    static public $data 		= array(); // beeinhaltet aktuelle Datens�tze als Array
    static public $tablename 	= "";
    static public $sort 		= false;
    static public $sortit 		= "";
    static public $tablefields 	= array();
   	static public $where 		= array();
   	static public $what 		= array();
   	static public $group 		= array();
   	static public $insertid		= "";
   	static public $join			= "";

    static public function get_data() {
        $sort = "";
        if (self::$sort) {
            $sort = "ORDER BY " . implode(",", self::$sort);
        }
    	if (self::$sortit) {
            $sortit = implode(" ", self::$sortit);
        } else {
        	$sortit = "";
        }
        
    	if (self::$what) {
        	$what = implode(" ", self::$what);
        } else {
        	$what = "*";
        }
        
    	if (self::$join) {
            $join = self::$join;
        } else {
        	$join = "";
        }
    
        $where = self::get_where();
    
        $sql = "SELECT $what FROM ".self::$tablename." $join $where $sort $sortit";
		#debug($sql);
    
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
			if(!$result) {
				die( mysql_error() );
			} 
        } else {
        	$result = mysql_query($sql);
        }
        if (!$result) {
        	
            self::$data = array();
            return;       
        }         
        
        self::$data = array();
     
        while ($row = mysql_fetch_assoc($result)) {
            //1. Eintrag des akt. Datensatzes ermitteln
            // und als Key fuer das DATA-Array verwenden:
          
            $key = current($row);
           
            self::$data[$key] = array_map("escapeRight",$row);   
        }
        
    } 
    

	static public function get_array() {
        $sort = "";
        
        if (self::$sort) {
            $sort = "ORDER BY " . implode(",", self::$sort);
        }
    	if (self::$sortit) {
            $sortit = implode(", ", self::$sortit);
        } else {
        	$sortit = "";
        }
        if (self::$what) {
        	$what = implode(" ", self::$what);
        } else {
        	$what = "*";
        }

        if (self::$group) {
        	$group = "GROUP BY ".implode(",", self::$group);
        } else {
        	$group = "";
        }
        
		if (self::$join) {
            $join = self::$join;
        } else {
        	$join = "";
        }
    
        $where = self::get_where();
    
        $sql = "SELECT $what FROM ".self::$tablename." $join $where $group $sort $sortit";
		#debug($sql);
	
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
        } else {
        	$result = mysql_query($sql);
        }
        if (!$result) {
        	die("Mysql Error: ".mysql_error());
            self::$data = array();
            return;       
        }

        self::$data = array();
     	#debug(mysql_fetch_assoc($result));  
        while ($row = mysql_fetch_assoc($result)) {
            //1. Eintrag des akt. Datensatzes ermitteln
            // und als Key fuer das DATA-Array verwenden:
            self::$data[] = array_map("escapeRight",$row);    
        }
    } 
    
    
    /**
     * Werte fuer Datenbanksortierung abfragen
     */
    static public function order_by($sortfeld) {
    	self::get_tablefields();
    	
        if(in_array($sortfeld, self::$tablefields)) {
            self::$sort[] = "$sortfeld";
        }        
    }
    
    /**
     * Ohne begrenzung der vorhandenen Feldnamen
     */
	static public function orders_by($sortfeld) 
	{
        self::$sort[] = "$sortfeld";
    }
    
    
	static public function sort($sortfeld) {
    	self::$sortit[] = $sortfeld;      
    }
    
    static public function get_tablefields() {
        $sql = "DESC " . self::$tablename;
    
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
        } else {
        	$result = mysql_query($sql);
        }
        if( $result ) {
	        while ($row = mysql_fetch_assoc($result)) {
	            self::$tablefields[] = $row["Field"];   
	        }  
        }       
    }  
  	
    /**
     * Werte fuer Where-Bedinugn abfragen
     */
    static public function set_where($fieldname, $condition, $value) {
        $where = array();
        $where["fieldname"] = $fieldname;
        $where["condition"] = $condition;
        $where["value"]     = $value;
        self::$where[] = $where;
    }

    /**
     * Where Bedingung zusammenbauen
     */
    static private function get_where() {
        $where = "";
        foreach (self::$where as $entry) {
            $where .= implode(" ", $entry);    
        }

        if ($where != "") {
            $where = " WHERE (" . $where . ")"; 
        }
        return $where;
    }
    
    /**
     * Text saeubern
     */
    static function cleartext($value) {
    	#$filter = array("<br />","&euro;","&uuml;","&Uuml;","&ouml;","&Ouml;","&auml;","&Auml;","&szlig;","&copy;","&trade;","&bdquo;","&ldquo;","&sbquo;","&lsquo;","&hellip;","&ndash;","&rsquo;","&rdquo;");
    	#$bad	= array("<br>","€","ü","Ü","ö","Ö","ä","Ä","ß","©","™","„","“","‚","‘","…","–","’","”");
    	
    	
    	#$value = str_replace($bad,$filter,$value);
    	#$value = strip_tags($value);
    	$value = stripslashes($value);
    	$value=htmlspecialchars($value);
    	#$value = htmlentities($value);
    	
    	#$value = $value;
    	
    	$value = mysql_escape_string($value);
    	return $value;
    }
    
    /**
     * Werte fuer Datenbankeintrag abfragen
     */
    static public function insert($fieldname, $value) {
    	$where = array();
        $where["fieldname"] = $fieldname;
        $where["value"]     = table::cleartext($value);
        self::$what[] 		= $where;
	}
	
	/**
	 * Datenbankeintrag anlegen
	 */
	static public function insertInto() {    
        $what = self::$what;
        $where = array();
        $wha = array();
        foreach($what as $var) {
        	$where[] = $var["fieldname"];
        	$wha[]   = $var["value"];
        }
        $where = implode(",",$where);
        $wha = implode("','",$wha);
    
        $sql = "INSERT INTO ".self::$tablename." ($where)VALUES('".$wha."')";
        #debug($sql);
	
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
        } else {
        	$result = mysql_query($sql);
        }
        
        self::$insertid = mysql_insert_id();
        
        if ($result) {
            return true;       
        } else {
        	die("MySQL Fehler: ".mysql_error());
        	return false;
        }         
    }
    
    /**
     * Datenbankeintrag bearbeiten
     */
	static public function update() {    
        $what = self::$what;
        $where = self::get_where();
        $wha = array();
        foreach($what as $var) {
        	$wha[] = $var["fieldname"]."='".$var["value"]."'";
        }
        $wha = implode(", ",$wha);
    
        $sql = "UPDATE ".self::$tablename." SET $wha $where";
		#debug($sql);
	
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
        } else {
        	$result = mysql_query($sql);
        }
        if ($result) {
            return true;       
        } else {
        	die(htmlentities($sql)."<br />".mysql_error());
        	return false;
        }         
    }
    
    /**
     * Datenbankeintrag entfernen
     */
	static public function delete() {    
        $where = self::get_where();
    
        $sql = "DELETE FROM ".self::$tablename." $where";
		#debug($sql);
		
        if( func::$mysqldebug ) {
        	$result = self::debug_mysql_query($sql);
        } else {
        	$result = mysql_query($sql);
        }
        if ($result) {
            return true;       
        } else {
        	return false;
        }         
    }
    
    /**
     * Zuletzt eingefügte ID ermitteln
     */
    static public function insertID() {
    	return self::$insertid;
    }
    
    
	static public function debug_mysql_query($query)
	{
		$js_query = str_replace(array('\\', "'"), array("\\\\", "\\'"), $query);
		$js_query = preg_replace('#([\x00-\x1F])#e', '"\x" . sprintf("%02x", ord("\1"))', $js_query);
		view::$data["debug"][] = '<script type="text/javascript">console.log("'.$js_query.'");</script>'."\n";
		return mysql_query($query);
	}
	
    
    /**
     * Variablen leeren
     */
    static public function reset() {
        self::$data 	= array();
        self::$where 	= array();
        self::$what 	= array();
        self::$group 	= array();
        self::$sort 	= false;
        self::$sortit 	= false;
        self::$insertid	= false;
        self::$join		= false;
    }    
}
?>