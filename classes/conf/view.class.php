<?php
class view
{
    static public $template = "";   
    static public $data = array();
 
	/**
	 * Funktion, welche die komplette Seite rendert
	 */
    static public function render() {
        extract(self::$data); // Assoz. Array wird in enzelne Variablen zerlegt
		if( self::$template ) {
        	include(self::$template);
		}
    }
}
?>