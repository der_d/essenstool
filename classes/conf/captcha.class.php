<?php
session_start();
Header("Content-Type: image/png");

#########################################################
################### wichtige Angaben ####################

$breite = 120;  # die Breite des Rechtecks
$hoehe  = 21;   # die H�he des Rechtecks


#  das Originalbild  #
$bild   = "../../themes/images/image.jpg";
######################


#########################################################

# Vorhandenen Bild , welches Hintergrund darstellen soll laden
$img = ImageCreateFromJPEG($bild);

# Originalgr��e des Bilds ermitteln
$orgbildgroesse = getimagesize($bild);

# neues Bild mit oben gesetzten gr��enangaben erstellen
$neubild = imagecreatetruecolor($breite,$hoehe);


# Schriftfarben #
$color = ImageColorAllocate($neubild, 0, 0, 0); # Hier wird der Variable $color_1 die Farbe weiss (RGB) zugewiesen
$grey = ImageColorAllocate($neubild, 131, 139, 131); # Hier wird der Variable $color_1 die Farbe weiss (RGB) zugewiesen

# diverse Hoehen- und Breitenangaben
$dstX = 0;
$dstY = 0;
$srcX = 0;
$srcY = 0;
$dstW = $breite;
$dstH = $hoehe;
$srcW = $orgbildgroesse[0];
$srcH = $orgbildgroesse[1];

# bestimmten Bildbereich aus Originalbild in neues Bild kopieren
imagecopyresampled ($neubild, $img, $dstX, $dstY, $srcX, $srcY, $dstW, $dstH, $srcW, $srcH);

# Hier wird die Schrifth�he mit 5 belegt (hier k�nnt ihr mit den Werten rumprobieren)
$font_height = ImageFontHeight(5);

# Hier wird die Schriftbreite mit 5 belegt (hier k�nnt ihr mit den Werten rumprobieren)
$font_width = ImageFontWidth(5); 

# $image_height mit der max H�he des Bildes deffinieren
$image_height = $hoehe;  

# $image_width mit der max Breite des Bildes deffinieren
$image_width = $breite;


# Array, welches die Buchstaben enth�lt welche auf da Bild sollen
$array = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0");


$key = "";              # leeren Key erstellen
$anz = count($array);   # Arrayelemente zaehlen


# Key mit 10 Buchstaben, Pixeln und Linien fuellen
for($i=0;$i<=6;$i++)
{
    $key .= $array[rand(0,$anz)];
}   

for($i = 0; $i <= 10; $i++) {
    $pointLeft = rand(1, $breite);
    $pointTop = rand(1, $hoehe);
    
    $pointLeftLine = rand(($i+10), $breite);
    $pointRightLine = rand(($i+1), $breite);
    
    $pointTopLine = rand(1, $hoehe);
    $pointBottomLine = rand(1, $hoehe);

    imagesetpixel($neubild, $pointLeft, $pointTop, $grey);
    imageline($neubild, $pointLeftLine, $pointTopLine, $pointRightLine, $pointBottomLine, $grey);
}

# Session mit key fuellen
$_SESSION["captcha"] = $key;

# Schriftbreite an Bild angepassen 
$length = $font_width*strlen($key);

# Hier kriegt man durch Teilungen die Mitte des Bildes heraus #
$image_center_x = ($image_width/2)-($length/2);
$image_center_y = ($image_height/2)-($font_height/2); 

# Key auf Bild schreiben
ImageString($neubild, 5, $image_center_x, $image_center_y, $_SESSION["captcha"], $color); 


ImagePNG($neubild);     # PNG Bild erstellen
ImageDestroy($neubild)  # Bildinhalt zerst�ren
?>