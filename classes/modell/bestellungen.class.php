<?php
class bestellungenModell
{
	
	static public function uebersicht()
	{
		table::reset();															// vorherige DB-Abfrage loeschen															//
        table::$tablename = "eat_bestellung";       								// Verbindung mit Tabelle 'bestellung' herstellen
        table::set_where("benutzerid","=","'".func::logged()."'");
        table::order_by("date");												// Sortierung nach name
		table::sort("DESC LIMIT 0,10");
        table::get_array();
		
		if( count(table::$data) > 0 ) {
	  		return table::$data;
	  	} else {
			return false;	  		
	  	}
	}
	
	static public function positions($id)
	{
		table::reset();															// vorherige DB-Abfrage loeschen															//
	    table::$tablename = "eat_bestellung_position";      						// Verbindung mit Tabelle 'bestellung' herstellen
	    table::set_where("id_bestellung","=","'$id'");
		table::get_array();
		
		if( count(table::$data) > 0 ) {
	  		return table::$data;
	  	} else {
			return false;	  		
	  	}
	}
}