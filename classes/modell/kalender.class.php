<?php

class kalenderModell
{
	public function data($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','>=', "'$start'");
		table::set_where(' AND date','<=',"'$end'");
		table::order_by("date");
		table::get_data();
		
		return table::$data;
	}
	
	public function menu($date,$id)
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','=', "'$date'");
		table::set_where(' AND id','=',"'$id'");	

		table::get_array();

		if( count(table::$data) > 0 ) {		
			return table::$data[0];
		} else {
			return false;
		}
	}
	
	public function bestellt($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_bestellung as b, eat_menu as m';
		table::set_where('m.id','=','b.menu');
		table::set_where(' AND b.date','>=', "'$start'");
		table::set_where(' AND b.date','<=',"'$end'");
		table::set_where(' AND b.user','=',"'".func::logged()."'");
		table::order_by("m.date");
		table::get_data();
	
		return table::$data;
	}
	
	public function menues($date)
	{
		table::reset();
		table::$tablename = 'eat_menu as m';
		table::set_where('m.date','=',"'$date'");
		table::get_data();
		
		return table::$data;
	}
	
	public function userOrders($date)
	{
		table::reset();
		table::$what = array('m.id as menu,b.id as id,b.name as name');
		table::$tablename = 'eat_bestellung as o, eat_benutzer as b, eat_menu as m';
		table::set_where('b.id','=',"o.user");
		table::set_where(' AND m.id','=',"o.menu");
		table::set_where(' AND o.date','=',"'$date'");
		table::get_array();
		
		return table::$data;
	}
	
	public function status($kw) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::set_where('kw','=', "'$kw'");
		table::get_data();
		
		$array = array();
		foreach(table::$data as $var) {
			$array[$var['kw']] = $var['open'];
		}
		return $array;
	}
	
	public function insert() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::insert('menu',$_POST['value']);
		table::insert('user',func::logged());
		table::insert('date',$_POST['option']);
		
		table::insertInto();
	}
	
	public function delete() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('menu','=',$_POST['value']);
		table::set_where(' AND user','=',func::logged());
		table::set_where(' AND date','=',$_POST['option']);
		
		table::delete();
	}	
}
