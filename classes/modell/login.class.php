<?php
class LoginModell
{
	static function linkfordernCheck() 
	{
		table::reset();													// vorherige DB-Abfrage leeren
	    table::$tablename = "eat_benutzer";       							// Verbindung mit Tabelle 'benutzer' herstellen
	    table::set_where("email","=","'".$_POST["mail"]."'");			// Benutzermail abfragen
	    
	    table::get_array();												// Datenbankdaten anfordern
	    
	    if( count(table::$data) > 0 ) {
	    	return true;
	    } else {
	    	return false;
	    }
	}
	
	static function linkfordern()
	{
		$array = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
		    		   "0","1","2","3","4","5","6","7","8","9");
		    	
    	$code = "";									// Leeres Array fuer Sicherheitscode
    	for($i=1;$i<=20;$i++) {						// For-Schleife fuer 20 Zeichen langen Sicherheitscode
    		$element = rand(0,(count($array)-1));			// Zufallszahl anhand der Zeichen im Array ermitteln
    		$case = rand(1,2);							// Zufallszahl fuer gross- kleinschreibung ermitteln
    		
    		if($case==1) {								// Wenn Zufallszahl gleich 1
    			$code .= strtoupper($array[$element]);		// Zufallszeichen aus Array auslesen und Gro�geschrieben an Variable anhaengen
    		} else {									// sonst
    			$code .= $array[$element];					// Zufallszeichen aus Array auslesen und Kleingeschrieben an Variable anhaengen
    		}											// Ende
    							
    	}
    	
    	table::reset();
    	table::$tablename = "eat_benutzer";
    	table::insert("code",$code);				// Code Speichern
		
    	table::set_where("email","=","'".$_POST["mail"]."'");
    	
    	if( isset($_POST["mail"]) && $_POST["mail"] != "" ) {
    		table::update();
    		return $code;
    	} else {
    		return false;
    	}
	}
	
	static function speichern() 
	{
		table::reset();											// vorherige DB-Abfrage leeren
	    table::$tablename = "eat_benutzer";       					// Verbindung mit Tabelle 'benutzer' herstellen
	    
	    table::insert("email",$_POST['mail']);					// E-Mail-Adresse einfuegen
	    table::insert("password",md5($_POST["password"]));		// Passwort einfuegen
		table::insert("name",$_POST['name']);					// Name einfuegen
	    table::insertInto();									// Daten in Datenbank speichern
	    
    	return true;
	}
	
	static function userExists()
	{
		table::reset();											// vorherige DB-Abfrage leeren
	    table::$tablename = "eat_benutzer";       					// Verbindung mit Tabelle 'benutzer' herstellen
	    
	    table::set_where("email","=","'".$_POST['mail']."'");	// E-Mail-Adresse einfuegen
	    table::get_data();										// Daten in Datenbank speichern
  
    	return table::$data;
	}
	
	static function pwspeichern() 
	{
		table::reset();													// vorherige DB-Abfrage leeren
	    table::$tablename = "eat_benutzer";       							// Verbindung mit Tabelle 'benutzer' herstellen
	    table::set_where("email","=","'".$_POST["mail"]."'");		// Benutzermail abfragen
	    
	    table::insert("code","");
	    table::insert("password",md5($_POST["password"]));
	    table::update();
	    
    	return true;
	}
}