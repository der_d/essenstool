(function($) {
	basepath = $('#base').val();

	/* Inputs leeren, die Klassenname leeren besitzen*/
	$(function(){
		var inputs = $.find("input.leeren");
		$(inputs).each(function() {
			var $this = $(this);
			
			if( $this.val() != "" ) {
		        $this.removeClass("sprite").css("background-position","top left");
		    }
		});
	});


	$('input.leeren').focus(function() {
		var $this = $(this);
		
		if ( $this.val() == "" ){
			$this.removeClass("sprite");
		}
		var next = $this.next('input.leeren');
		if( next.val() != "" ) {
		   next.removeClass("sprite");	
		}
	}).blur(function() {
		var $this = $(this);
		if ( $this.val() == "" ){
			$this.addClass("sprite");
		}
		var next = $this.next('input.leeren');
	    if( next.val() != "" ) {
	       next.removeClass("sprite");  
	    }
	});


	/* Inputs leeren, die Klassenname clearing besitzen */
	$('input.clearing').focus(function() {
		var $this = this;
	    if ($this.value == $this.defaultValue){
	        $this.value = '';
	    }
	}).blur(function() {
		var $this = this;
	    if ($this.value == ''){
	        $this.value = ($this.defaultValue ? $this.defaultValue : '');
	    }
	});

	
	
	/* nachfolgenden DIV oeffnen */
	$("a.expand").click( function(e) {
		var $this = $(this),
			next  = $this.parent("div").next(".user");
		
		if( $(next[0]).is(":hidden") ) {
			$(next[0]).slideDown("slow");
			$this.html('Tag ausblenden');
		} else {
			$(next[0]).slideUp("slow");
			$this.html('Tag anzeigen');
		}
		e.preventDefault();
		return false;
	});


    /* Hover fuer Buttons */
	$(".btn, .butn").hover(function() {
		$(this).addClass("btnHover hover");
	}, function(){
		$(this).removeClass("btnHover hover");
	});
	/* Click auf nicht Link-Bereich des Links */
	$("span.btn").click(function(e){
		var link = $(this).find("a");
		location.href = $(link).attr("href");
		e.preventDefault();
		return false;
	});

	
	
	/* Ajax-Request fuer warenkorb */
    $("#warenkorbLink").click(function(e) {
        var $this = $(this).attr('href'),
        	kw = $this.split('#');
        
        if( $("#warenkorb").hasClass('hidden') ) {
	        $.ajax({
	          	url: basepath + "/index.php?zeige=warenkorb",
	          	global: false,
	          	type: "POST",
	          	data: ({q : kw[1]}),
	          	dataType: "html",
	          	async:true,
	          	success: function(msg){
	          		if( msg == 'error' ) {
          				document.location.href = basepath;
	          		} else {
	          			value = msg.split('###');
	          			
		            	$("#warenkorb")
		            		.html(value[0])
		            		.removeClass('hidden');
		           }
	        	}
			});
		} else {
			$('#warenkorb').addClass('hidden');
		}
		e.preventDefault();
		return false;
    });

	
	/* Ajax-Request fuer bestellen */
    $("#kalender.front input.select").click(function(e) {
        var $this = $(this),
        	value = $this.attr('value'),
        	option = $this.attr('name'),
        	className = $this.attr('alt');
        
        if( $this.is(':checked') ) {
        
        	$('.' + className).addClass('ordered');
        
	        $.ajax({
	          	url: basepath + "/index.php?zeige=insert",
	          	global: false,
	          	asynch:true,
	          	cache:true,
	          	type: "POST",
	          	data: ({'value':value,'option':option}),
	          	dataType: "html",
	          	async:true,
	          	success: function(msg){
	          		
	          		value = msg.split('###');
	          		
	          		if(value[0] == 'error') {
	          			 document.location.href = basepath;
	          		} else {
	          			if( value[0] == 'nicht mehr Bestellbar!' ) {
	          				$('.' + className).removeClass('ordered');
	          				$this
	          					.removeAttr('checked')
	          					.attr('disabled','disbaled');
	          			}
	          			
		            	$("#warenkorb")
		            		.html(value[0])
		            		.removeClass('hidden');
		            	$("#warenkorbLink").html("Warenkorb (" + value[1] + ")");
	            	}
	        	}
			});
		} else {
			
			$('.' + className).removeClass('ordered');
			
			$.ajax({
				url: basepath + "/index.php?zeige=delete",
	          	global: false,
	          	asynch:true,
	          	cache:true,
	          	type: "POST",
	          	data: ({'value':value,'option':option}),
	          	dataType: "html",
	          	async:true,
	          	success: function(msg){
	          		
	          		value = msg.split('###');
	          		
	          		if(value[0] == 'error') {
	          			 document.location.href = basepath;
	          		} else {
		            	$("#warenkorb")
		            		.html(value[0])
		            		.removeClass('hidden');
		            	$("#warenkorbLink").html("Warenkorb (" + value[1] + ")");
	            	}
	        	}
	        });
		}		
    });
	
	
	$("#newPW").submit(function(e) {
		if( $("#password").val() != $("#repeatPW").val() ) {
			$("#repeatPW").after("<span id='pwerror' class='error'>Passwort inkorrekt!</span>");
			$("#pwerror")
			.animate(
				{opacity:1},
				2000)
			.animate(
				{opacity:0},
				2000,
				function() {
					$("#pwerror").remove();
				}
			);
			e.preventDefault();
			return false;
		} else if( $("#repeatPW").val() == "" ) {
			$("input[name='einloggen']").before("<span id='formError' class='error display mb4'>Bitte Formular ausf&uuml;llen!</span>");
			$("#formError")
				.animate(
					{opacity:1},
					2000)
				.animate(
					{opacity:0},
					2000,
					function() {
						$("#formError").remove();
					}
				);
			e.preventDefault();
			return false;
		} else {
			return true;
		}
	});
	
	
	/** Tooltip **/
	this.vtip = function() {    
	    this.xOffset = -15; // x distance from mouse
	    this.yOffset = -56; // y distance from mouse       
	    
	    $(".vtip").unbind().hover(    
	        function(e) {
	            this.t = this.title;
	            this.title = ''; 
	            this.top = (e.pageY + yOffset); this.left = (e.pageX + xOffset);
	            
	            $('body').append( '<div id="vtip"><div class="vtipArrow icons"></div>' + this.t + '</div>' );
	                        
	            $('div#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("slow");
	        },
	        function() {
	            this.title = this.t;
	            $("div#vtip").fadeOut("slow").remove();
	        }
	    ).mousemove(
	        function(e) {
	            this.top = (e.pageY + yOffset);
	            this.left = (e.pageX + xOffset);
	                         
	            $("div#vtip").css("top", this.top+"px").css("left", this.left+"px");
	        }
	    );            
	};
	jQuery(document).ready(function($){vtip();}) 	
	
	
	if ( $('.popup').length ) {
	    var width = $(window).width(),
	        height= $(window).height(),
	        posLeft = (width / 2) - ($('.popup').width() / 2),
            posTop = (height / 2) - ($('.popup').height() / 2);
	        
        $('.popupBackground').css({
	        'width' : width,
	        'height' : height
        });
        $('.popup').css({
            'left' : posLeft,
            'top' : posTop
        });
	}
	
	if ( $('.popup').length && ($.cookie('eatVisit') != null && $.cookie('eatVisit') == 0) ) {
	   $('.popup,.popupBackground').fadeIn(1000);
	}
	
	$('.closePopup').click(function() {
	    var date = new Date(),
	        expiry = date;   
        expiry.setTime(date.getTime()+(10*60*1000));

	    $.cookie('eatVisit', '1', { expires: expiry.getTime() });
	    
	    $('.popup,.popupBackground').fadeOut(200);
	});
	
	$('#pricePopup').click(function() {
        $('.popup,.popupBackground').fadeIn(200);
        return false;
    });
	
	
    //$("#pricePopup").effect("pulsate");
	
})(jQuery);