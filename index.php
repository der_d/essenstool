<?php
require_once 'classes/config.class.php';
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();

header ("content-type: text/html; charset=UTF-8");
ini_set('default_charset', 'UTF-8');
ini_set('max_execution_time', 0);
set_time_limit(180);
error_reporting(E_ALL);
//ini_set('display_errors', 1);

header ("cache-control: must-revalidate");
$offset = 48 * 60 * 60;
$expire = "expires: " . gmdate ("D, d M Y H:i:s", time() + $offset) . " GMT";
header ($expire);

ini_set("session.gc_maxlifetime", "18000");

session_start();
ini_set("url_rewriter.tags","a=href,area=href,frame=src,input=src,fieldset=");


/**
 * gibt Arrays und andere Daten mit PRE Formatiert zurueck
 * @param string|array $code
 */
function debug($code)
{
    echo "<pre>";
    print_r($code);
    echo"</pre>";
}


/**
 * Laed eine entsprechende Datei und deren Methoden
 * @param object $className
 */
function __autoload($className)
{
	if (file_exists('classes/app/'.$className.'.class.php')) {
		require_once 'classes/app/'.$className.'.class.php';
	} else {
		require_once 'classes/conf/error.class.php';
		$meine_webseite = new error($script_path,$seoURL,$mailadress);
		exit;
	}

	if(file_exists('classes/modell/'.$className.'.class.php')) {
		require_once 'classes/modell/'.$className.'.class.php';
	}
}
require_once 'classes/conf/func.class.php';
// System
func::$seoURL 		= $seoURL;						// SEO-URL (1|0) abfragen
func::$scriptPath 	= $script_path;					// Scriptpfad abfragen
func::$dirPath		= $dirPath;
func::$unkostenpauschale = $unkostenpauschale;
func::$unkostenbeitrag   = $unkostenbeitrag;



// Entwicklungsumgebung
func::$development	 = $development;
func::$mysqldebug	 = $mysqldebug;

require_once("classes/conf/view.class.php");
require_once("classes/conf/database.class.php");
require_once("classes/conf/tables.class.php");

require_once("classes/modell/index.class.php");

if (!login::isLoggedIn()) {
    $login = new login();
    $login->check(true);
}

/**
 * Hauptklasse, in der saemtliche aktionen abgefeuert werden
 * @author Danny Curth
 */
class Application
{
    private $db 			= "";
    private $scriptPath 	= "";
    public $script_array 	= "";
    static public $seoURL	= "";
    static public $script	= "";

	static protected $kw	= "";

	static protected $login = "";


    /**
     * Hauptkonstruktor fuer alle weiteren Methoden
     * @param string $script_path
     * @param string $seoURL
     * @param string $mail
     */
    public function __construct($script_path,$seoURL,$mail)
    {
    	// Entwicklungsumgebung ja/nein
    	view::$data["development"] = func::$development;

    	// Abfragen ob SEO-Optimierte URLs gebaut werden sollen
    	$this->seoURL = $seoURL;

    	// Scriptpfad einlesen:
    	$this->scriptPath = $script_path;

    	// Standart Mailadresse einlesen:
    	$this->mail	= $mail;

        // Datenbank initialisieren:
        $this->db = new Database();



        // View initialisieren:
        view::$template = "views/template.phtml";

        view::$data["script_path"] 		= $this->scriptPath;

		view::$data['userdata'] = func::userdata();


		view::$data["login"] 			= func::writeURL("modul=login,aktion=check",$seoURL,$script_path);
		view::$data["logout"] 			= func::writeURL("modul=login,aktion=logout",$seoURL,$script_path);
		view::$data["createPassword"] 	= func::writeURL("modul=login,aktion=passwort",$seoURL,$script_path);


		// Benutzerdaten auslesen und an View uebergeben
		if(func::logged()) {
			view::$data["user"] 		= func::userdata();
		}

		// SEO URL auslesen
        $this->script_array = func::readURL($this->scriptPath,$this->seoURL);
        self::$script = $this->script_array;

		$ziel = ( isset($this->script_array['modul']) && $this->script_array['modul'] != '' ) ? $this->script_array['modul'] : 'kalender';

		// KW an View uebergeben
		self::$kw						= ( isset($this->script_array['kw']) && $this->script_array['kw'] != '' ) ? $this->script_array['kw'] : date('W');
		self::$kw = (self::$kw < 10) ? '0'.(int)self::$kw : self::$kw;

		view::$data["kw"] 				= self::$kw;

		// Startdatum aus KW
        $startDay  = strtotime(date('Y-\\W'.self::$kw));
        $endDay    = strtotime("+5 day",$startDay);

        // Anz. Bestellungen an View
        view::$data['anzOrders'] = indexModell::anzWarenkorb($startDay,$endDay);

		self::$login = func::logged();

		// wenn Warenkorb geoeffnet werden soll
		$getArray = array('warenkorb','insert','delete');

		if ( isset($_GET['zeige']) && in_array($_GET['zeige'],$getArray) ) {
			$this->$_GET['zeige']();
			return;
		}

		$this->login();

		////////////////////////////
		// Seite Laden:           //
		////////////////////////////
		view::$data["aktion"] = isset($this->script_array["aktion"]) ? $this->script_array["aktion"] : "";
        view::$data["modul"] = $ziel;
		$this->geheZu($ziel);
  	}

  	public function geheZu($ziel = "")
  	{
  		$this->$ziel();
  	}

	public function __get($varname)
	{
        return $this->$varname;
    }

    public function __set($varname, $value)
    {
        $this->$varname = $value;
    }

    /**
     * Template fuer Seiten aufrufen, wo kein Zugriff erlaubt ist
     */
	public function verboten()
	{
        view::$data["seitentitel"] 	= "Zugriff verweigert";					// Seitentitel an View uebergeben
        view::$data["content"] 	 	= "views/system/accessdenied.phtml";	// Contenttemplate an View uebergeben
        view::render();														// Template rendern
    }


    /********************************/
    /* Module************************/
    /********************************/
    private function login()
    {
    	if( func::logged() ) {
    		view::$data["loginbox"] = "views/modules/logged.phtml";
    	} else {
    		view::$data["loginbox"] = "views/modules/login.phtml";
    	}
    }

	private function warenkorb()
    {
        if( self::$login ) {
            $kw = $_POST['q'];
            $startDay  = strtotime(date('Y-\\W'.$kw));
            $endDay    = strtotime("+5 day",$startDay);

            view::$data["data"] = kalenderModell::bestellt($startDay,$endDay);

            view::$data["status"] = indexModell::payStatus($kw,func::logged());


            view::$data["kw"] = $kw;
            view::$template = 'views/warenkorb/index.phtml';
            view::render();

            echo "###".indexModell::anzWarenkorb($startDay,$endDay);
        } else {
            echo 'error###0';
        }
    }

    private function insert()
    {
        if( self::$login ) {
            $kw = date('W',$_POST['option']);
            $startDay  = strtotime(date('Y-\\W'.$kw));
            $endDay    = strtotime("+7 day",$startDay);

            $data = kalenderModell::menu($_POST['option'],$_POST['value']);

            if( $data ) {
                if( time() < $data['date'] && $data['id'] != '' ) {
                    kalenderModell::insert();

                    $data = kalenderModell::bestellt($startDay,$endDay);

                    view::$data["data"] = $data;

                    view::$data["kw"] = $kw;
                    view::$template = 'views/warenkorb/index.phtml';
                    view::render();

                    echo "###".indexModell::anzWarenkorb($startDay,$endDay);
                } else {
                    echo 'nicht mehr Bestellbar!###0';
                }
            } else {
                echo 'error###0';
            }
        } else {
            echo 'error###0';
        }
    }

    private function delete()
    {
        if( self::$login ) {
            $kw = date('W',$_POST['option']);
            $startDay  = strtotime(date('Y-\\W'.$kw));
            $endDay    = strtotime("+7 day",$startDay);

            $data = kalenderModell::menu($_POST['option'],$_POST['value']);

            if( $data ) {
                kalenderModell::delete();

                $data = kalenderModell::bestellt($startDay,$endDay);

                view::$data["data"] = $data;

                view::$data["kw"] = $kw;
                view::$template = 'views/warenkorb/index.phtml';
                view::render();

                echo "###".indexModell::anzWarenkorb($startDay,$endDay);
            } else {
                echo 'error###0';
            }
        } else {
            echo 'error###0';
        }
    }
}

$script = func::readURL();
$script = (!empty($script['modul'])) ? $script['modul'] : "kalender";

$meine_webseite = new $script($script_path,$seoURL,$mailadress,$dirPath);
?>