<?php
require_once '../classes/config.class.php';			// Config einbinden
header('content-type: text/html; charset=utf-8');

session_start();

error_reporting(E_ALL);

/**
 * Funktion fuer Testausgaben
 */
function debug($code) {
    echo "<pre>";
    print_r($code);
    echo"</pre>";
}

/**
 * Klassendatei aufrufen und neue Klasse erstellen
 */
function __autoload($className) {
	if(file_exists('classes/app/'.$className.'.class.php')) {
		require_once 'classes/app/'.$className.'.class.php';
	} else {
		require_once 'classes/conf/error.class.php';
		$meine_webseite = new error($script_path,$seoURL,$dirPath); // FIXME!
		exit;
	}

	if(file_exists('classes/modell/'.$className.'.class.php')) {
		require_once 'classes/modell/'.$className.'.class.php';
	}
}

require_once '../classes/conf/func.class.php';		// Funktionen einbinden

// System
func::$seoURL 		= $seoURL;						// SEO-URL (1|0) abfragen
func::$scriptPath 	= $script_path;					// Scriptpfad abfragen
func::$dirPath		= $dirPath;

// Entwicklungsumgebung
func::$development	 = $development;
func::$mysqldebug	 = $mysqldebug;

func::$unkostenpauschale = $unkostenpauschale;
func::$unkostenbeitrag   = $unkostenbeitrag;

require_once("../classes/conf/view.class.php");		// View-Klasse einbinden
require_once("../classes/conf/database.class.php");	// Datenbank-Klasse einbinden
require_once("../classes/conf/tables.class.php");	// MYSQL-Abfrage-Klasse einbinden

class Application
{
    private $db = "";
    private $scriptPath 	= "";
    private $script_array 	= "";
    static public $seoURL	= "";
    static public $dirPath	= "";

	static protected $kw	= "";

	protected $userdata		= "";

    /**
     * Konstruktor, welcher diverse Funktionalitaeten bereitstellt
     * und Methoden zum Methodenaufruf (xD) bereithaellt
     */
    public function __construct($script_path,$seoURL,$dirPath) {
    	$this->seoURL 	= $seoURL;												// SEO-URL (1|0) auslesen

    	$this->scriptPath 	= $script_path;										// Scriptpfad einlesen

    	$this->dirPath		= $dirPath;


        $this->db = new Database();	 											// Datenbank initialisieren

        $this->userdata = func::userdata();


        view::$data["script_path"] 	= $this->scriptPath;						// Script-Pfad an View uebergeben

        $this->script_array = func::readAdminURL($this->scriptPath,$this->seoURL); // SEO-URL auselsen
        $link = $this->script_array;

		$ziel = (isset($this->script_array["modul"]) && $this->script_array["modul"]!="") ? $this->script_array["modul"] : "kalender";


		// KW an View uebergeben
		self::$kw						= ( isset($this->script_array['kw']) && $this->script_array['kw'] != '' ) ? $this->script_array['kw'] : date('W');
		self::$kw = (self::$kw < 10) ? '0'.(int)self::$kw : self::$kw;
		view::$data["kw"] 				= self::$kw;

		$startDay  						= strtotime(date('Y-\\W'.self::$kw));

		view::$data['nextKw']			= self::$kw;
		view::$data['year']				= date('Y');


        view::$data["modul"] = $ziel;											// Ziel an View uebergeben

    	$this->login();

        if( (isset($this->userdata['rights']) && $this->userdata['rights']==1) ) {
        																		// Fragen ob noetige Rechte besitzt
       		view::$template = "views/template.phtml"; 								// View initialisieren:
			$this->geheZu($ziel);													// Methodenaufruf
        } else {																// Sonst
            view::$template = "views/login/login.phtml";							// Template an View uebergeben
        	$this->geheZu($ziel);													// Methodenaufruf
        }																		// Ende
  	}

  	public function geheZu($ziel = "") {
  		$this->$ziel();
  	}

	public function __get($varname) {
        return $this->$varname;
    }

    public function __set($varname, $value) {
        $this->$varname = $value;
    }


    /**
     * Template fuer Seiten aufrufen, wo kein Zugriff erlaubt ist
     */
    public function verboten() {
        view::$data["seitentitel"] 	= "Zugriff verweigert";					// Seitentitel an View uebergeben
        view::$data["content"] 	 	= "views/system/accessdenied.phtml";	// Contenttemplate an View uebergeben
        view::render();														// Template rendern
    }

	/**
     * Template fuer Seiten aufrufen, wo kein Zugriff erlaubt ist
     */
    public function access() {
        view::$data["seitentitel"] 	= "Zugriff verweigert";					// Seitentitel an View uebergeben
        view::$data["message"]		= "Sie verfügen nicht über die nötigen Rechte um diese Funktion zu nutzen!";
        																	// MEssage an View uebergeben
        view::$data["content"] 	 	= "views/system/message.phtml";		// Template an View uebergeben
        view::render();														// Template rendern
    }

	/********************************/
    /* Module************************/
    /********************************/
    private function login()
    {
    	if( func::logged() ) {
    		view::$data["loginbox"] = "views/modules/logged.phtml";
    	} else {
    		view::$data["loginbox"] = "views/modules/login.phtml";
    	}
    }
}

$script = func::readAdminURL();											// URL auslesen
$script = (!empty($script['modul'])) ? $script['modul'] : "kalender";	// Wenn nichts gesetzt, Methoe auf Startseite setzen
$meine_webseite = new $script($script_path,$seoURL,$dirPath);			// Klassenaufruf
?>