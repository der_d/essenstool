<?php 
class error extends Application
{
	/**
	 * Konstruktor, der den Konstruktor seiner Elternklasse aufruft
	 * wichtig, da in dem Konstruktor der Elternklasse entsprechende Werte zu finden sind
	 * 
	 * @param string $script_path
	 * @param string $seoURL
	 * @param string $mail
	 * @param string $dirPath
	 */
	public function __construct($script_path,$seoURL,$dirPath) 
	{  		
		$this->dirPath	= $dirPath;
  		$this->path 	= $script_path;
    	Application::__construct($script_path,$seoURL,$dirPath);
  	}
  	
  	/**
  	 * Funktion, die Methoden der Klasse aufruft
  	 */
  	public function geheZu() 
  	{
  		$script = func::readURL();
  		$script = (isset($script["fehler"])) ? $script["fehler"] : "viernullvier" ;
  		$this->$script();
  	}
	
  	/**
  	 * 404 Error
  	 */
	public function viernullvier() 
	{
		view::$data["seitentitel"] = "404 Fehler";				// Seitentitel an View uebergeben
        view::$data["content"] = "views/system/four.phtml";	// Content-Template an View uebergeben
        view::render();											// Template rendern
    }
}
?>