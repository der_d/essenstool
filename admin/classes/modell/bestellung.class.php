<?php

class bestellungModell
{
	public function data($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','>=', "'$start'");
		table::set_where(' AND date','<=',"'$end'");
		table::order_by("date");
		table::get_data();
		
		return table::$data;
	}
	
	public function menu($date,$id)
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','=', "'$date'");
		table::set_where(' AND id','=',"'$id'");	

		table::get_array();

		if( count(table::$data) > 0 ) {		
			return table::$data[0];
		} else {
			return false;
		}
	}
	
	public function bestellt($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_bestellung as b, eat_menu as m';
		table::set_where('m.id','=','b.menu');
		table::set_where(' AND b.date','>', "'$start'");
		table::set_where(' AND b.date','<',"'$end'");
		table::set_where(' AND b.user','=',"'".func::logged()."'");
		table::order_by("m.date");
		table::get_data();
	
		return table::$data;
	}
	
	public function status($kw) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::set_where('kw','=', "'$kw'");
		table::get_data();
		
		$array = array();
		
		foreach(table::$data as $var) {
			$array[$var['date']] = $var['open'];
		}
		return $array;
	}
	
	public function user() 
	{
		table::reset();
		table::$tablename = 'eat_benutzer';
		table::get_data();
		
		return table::$data;
	}
	
	public function bestellungen($id,$start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('user','=',"'".$id."'");
		table::set_where(' AND date','>=', "'$start'");
		table::set_where(' AND date','<=',"'$end'");
		table::get_data();
		
		return table::$data;
	}

	public function orders($start,$end)
	{
		table::reset();
		table::$what = array('m.*,count(o.menu) as gesamt');
		table::$tablename = 'eat_bestellung as o, eat_menu as m';
		table::set_where('o.menu','=',"m.id");
		table::set_where(' AND o.date','>=',"'$start'");
		table::set_where(' AND o.date','<=',"'$end'");
		table::$sortit = array('GROUP BY o.menu ORDER BY o.date');
		table::get_data();
		
		return table::$data;
	}
	
	public function userOrders($start,$end)
	{
		table::reset();
		table::$what = array('b.*, SUM(m.price) AS gesamt, COUNT(*) AS orders, s.noSurchargeCount AS nachtisch');
		table::$tablename = 'eat_benutzer AS b
                        LEFT JOIN eat_bestellung AS o ON b.id = o.user
                        LEFT JOIN eat_menu as m ON o.menu = m.id
                        LEFT JOIN (SELECT b.id AS user, count(*) AS noSurchargeCount
                                FROM eat_benutzer AS b 
                                LEFT JOIN eat_bestellung AS o ON b.id = o.user
                                LEFT JOIN eat_menu as m ON o.menu = m.id
                                 WHERE m.typ = 8
                                 GROUP BY b.id) AS s ON s.user = b.id';
		table::set_where('o.date','>=',"'$start'");
		table::set_where(' AND o.date','<=',"'$end'");
		table::$sortit = array('GROUP BY b.id ORDER BY b.name');
		table::get_data();
		
		return table::$data;
	}
	
	public function userStatus($kw)
	{
		table::reset();
		table::$tablename = 'eat_payed';
		table::set_where('kw','=',"'$kw'");
		table::get_data();
		
		return table::$data;
	}
	
	public function insert() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::insert('menu',$_POST['value']);
		table::insert('user',func::logged());
		table::insert('date',$_POST['option']);
		
		table::insertInto();
	}
	
	public function delete() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('menu','=',$_POST['value']);
		table::set_where(' AND user','=',func::logged());
		table::set_where(' AND date','=',$_POST['option']);
		
		table::delete();
	}
	
	
	public function payStatus($kw,$user)
	{
		table::reset();
		table::$tablename = 'eat_payed';
		table::set_where('userID','=',"'$user'");
		table::set_where(' AND kw','=',"'$kw'");
		table::get_data();

		if( isset(table::$data[$user]['status']) && table::$data[$user]['status'] == 1 ) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public function setPayed($kw,$user,$status)
	{
		table::reset();
		table::$tablename = 'eat_payed';
		table::insert('userID',$user);
		table::insert('kw',$kw);
		table::insert('status',$status);
		
		table::insertInto();
	}
	
	public function deleteKwOrders($start,$end)
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('date','>=',"'$start'");
		table::set_where(' AND date','<=',"'$end'");
		
		table::delete();
	}	
	
	public function insertSingleOrder($date,$user,$menu)
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::insert('menu',$menu);
		table::insert('date',$date);
		table::insert('user',$user);
		
		table::insertInto();
	}
}
