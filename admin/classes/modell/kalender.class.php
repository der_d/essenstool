<?php

class kalenderModell
{
	public function data($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','>=', "'$start'");
		table::set_where(' AND date','<=',"'$end'");
		table::order_by("date");
		table::get_data();
		
		return table::$data;
	}
	
	public function menu($date,$id)
	{
		table::reset();
		table::$tablename = 'eat_menu';
		table::set_where('date','=', "'$date'");
		table::set_where(' AND id','=',"'$id'");	

		table::get_array();

		if( count(table::$data) > 0 ) {		
			return table::$data[0];
		} else {
			return false;
		}
	}
	
	public function status($kw) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::set_where('kw','=', "'$kw'");
		table::get_data();
		
		$array = array();
		
		foreach(table::$data as $var) {
			$array[$var['date']] = $var['open'];
		}
		return $array;
	}
	
	public function dateStatus($date) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::set_where('kw','=', "'$date'");
		table::get_data();
		
		if( count(table::$data) > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function kwStatus($start,$end) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		#table::set_where('kw','>=', "'$start'");
		#table::set_where(' AND kw','<=',"'$end'");
		table::get_array();
		
		return table::$data;
	}
	
	public function insert() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::insert('menu',$_POST['value']);
		table::insert('user',func::logged());
		table::insert('date',$_POST['option']);
		
		table::insertInto();
	}
	
	public function delete() 
	{
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('menu','=',$_POST['value']);
		table::set_where(' AND user','=',func::logged());
		table::set_where(' AND date','=',$_POST['option']);
		
		table::delete();
	}
	
	public function setStatus($kw,$status) 
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::insert('open',"$status");
		table::set_where('kw','=',"'$kw'");
		
		table::update();
	}
	
	
	public function mailed($kw)
	{
		table::reset();
		table::$tablename = 'eat_datum';
		table::set_where('kw','=', "'$kw'");
		table::get_array();
	
		if( isset(table::$data[0]['mail']) && table::$data[0]['mail'] == 1 ) {
			return true;
		} else {
			return false;
		}
	}	
}
