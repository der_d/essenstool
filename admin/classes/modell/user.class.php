<?php

class userModell
{
	public function users() 
	{
		table::reset();
		table::$tablename = 'eat_benutzer';
		table::order_by("name");
		table::get_data();
		
		return table::$data;
	}
	
	public function userData($id)
	{
		table::reset();
		table::$tablename = 'eat_benutzer';
		table::set_where('id','=', "'$id'");

		table::get_array();

		if( count(table::$data) > 0 ) {		
			return table::$data[0];
		} else {
			return false;
		}
	}
	
	public function save($id) 
	{
		table::reset();
		table::$tablename = 'eat_benutzer';
		table::insert('email',$_POST['email']);
		table::insert('name',$_POST['name']);
		
		table::set_where('id','=', "'$id'");
		
		table::update();
	}
	
	public function delete($id) 
	{
		table::reset();
		table::$tablename = 'eat_benutzer';
		table::set_where('id','=', "'$id'");
		
		table::delete();
		
		table::reset();
		table::$tablename = 'eat_bestellung';
		table::set_where('user','=', "'$id'");
		
		table::delete();
	}
}
