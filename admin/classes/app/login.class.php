<?php 
class login extends Application
{
	/**
	 * Konstruktor, der den Konstruktor seiner Elternklasse aufruft
	 * wichtig, da in dem Konstruktor der Elternklasse entsprechende Werte zu finden sind
	 * 
	 * @param string $script_path
	 * @param string $seoURL
	 * @param string $mail
	 * @param string $dirPath
	 */
	public function __construct($script_path,$seoURL,$dirPath) 
	{  		
		$this->dirPath	= $dirPath;
  		$this->path 	= $script_path;
    	Application::__construct($script_path,$seoURL,$dirPath);
  	}
  	
  	/**
  	 * Funktion, die Methoden der Klasse aufruft
  	 */
  	public function geheZu() 
  	{
  		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
  		$script = $script["aktion"];
  		$this->$script();
  	}
	
	public function check() 
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		
        @$email      = $_POST["mail"];							// benutzername pruefen
        @$password   = md5($_POST["password"]);					// benutzerpin pruefen
        
        table::reset();											// vorherige DB-Abfrage leeren
        table::$tablename = "eat_benutzer";        			// Verbindung mit Tabelle 'administratoren' herstellen

        table::set_where("email", "=", "'$email'");				// Where login='$login' 
        table::set_where(" AND password", "=", "'$password'");	// AND password='$password';
        
        table::get_array();										// Daten in Array speichern

        if ( count(table::$data) == 1 ) {						// Wenn Datenbankabfrage einen Wert zurueckliefert
            $_SESSION["login"] 			= table::$data[0]["id"];					// setze Session auf Kundennummer
            
        	view::$template = "views/template.phtml";				// Template an View uebergeben
            view::$data["seitentitel"] = "Login erfolgreich";		// Seitentitel an View uebergeben
            
            
            view::$data["content"] 	= "views/login/login_ok.phtml";// Content-Template an View uebergeben
        	view::render();											// Template rendern
        	exit;													// Funktion verlassen
        } else {												// Sonst
        	view::$data["seitentitel"] 	= "Fehler";					// Seitentitel an View uebergeben
            view::$data["error"]		= "Login fehlgeschlagen";	// Fehlermeldung
            view::$data["content"] 		= "views/login/loginfehler.phtml";	
            														// Content-Template an View uebergeben
        	view::render();											// Template rendern
        }														// Ende
    }
               
    private function logout() 
    {
        unset($_SESSION['login']);								// Session loeschen
        
        view::$data["seitentitel"] = "Logout erfolgreich";		// Seitentitel an View uebergeben
        view::$data["content"] = "views/login/logout.phtml";	// Content-Template an View uebergeben
        view::render();											// Template rendern
    }
}
?>