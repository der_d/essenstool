<?php 
class admin extends Application
{
	/**
	 * Konstruktor, der den Konstruktor seiner Elternklasse aufruft
	 * wichtig, da in dem Konstruktor der Elternklasse entsprechende Werte zu finden sind
	 * 
	 * @param string $script_path
	 * @param string $seoURL
	 * @param string $mail
	 * @param string $dirPath
	 */
	public function __construct($script_path,$seoURL,$mail,$dirPath) 
	{  		
		$this->dirPath	= $dirPath;
		$this->mail		= $mail;
  		$this->path 	= $script_path;
    	Application::__construct($script_path,$seoURL,$mail,$dirPath);
  	}

	public function geheZu() 
	{
  		if(func::adminLogged()) {													// Abfragen, ob eingeloggt
	  		if(func::adminRight(func::adminLogged())) {								// Rechte abfragen
	  			$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesen
		  		$script = isset($script["aktion"]) ? $script["aktion"] : "alle";			// Abfragen, ob aktion gesetzt
		  		$this->$script();															// Methodenaufruf
	  		} else {																	// Sonst
	  			parent::access();															// Zugriff verweigert - Methode aufrufen
	  		}																			// Ende
	  	} else {																	// Sonst
  			parent::verboten();															// Error-Methode aufrufen
  		}																			// Sonst
  	}

  	
	public function speichern() 
	{  	   															
		if( isset($_POST["id"]) && $_POST["id"] != "" ) {
			$id = $_POST["id"];
		} else {
			$id = 0;
		}
		if( $id != 0 ) {											// Abfragen ob ID nicht 0
			table::reset();												// vorherige DB-Abfrage leeren
	    	table::$tablename = "eat_administratoren";       				// Verbindung mit Tabelle 'administratoren' herstellen
	    	table::set_where("id","=",$id);					// ID abfragen
	    	table::set_where(" AND superadmin","=","1");				// Superadmin abfragen
	    	table::get_array();
	    	
	    	$anz = count(table::$data);
		} else {
			$anz = 0;
		}
		if( $anz == 0 ) {
		    table::reset();												// vorherige DB-Abfrage leeren
		    table::$tablename = "eat_administratoren";       				// Verbindung mit Tabelle 'administratoren' herstellen
		    
		    if( isset($_POST) ) {
			    if(isset($_POST["id"]) && $_POST["id"]!=0) {			// Abfrage ob ID gesetzt
					table::set_where("id","=",$_POST["id"]);				// ID abfragen
					
					table::insert("login",$_POST["login"]);					// Login Speichern
					table::insert("password",md5($_POST["pw"]));			// Passwort Speichern
					table::insert("rechte",implode(":",$_POST["rechte"]));	// Rechte Speichern
					table::update();										// Updaten
					
					view::$data["message"] 	= "Administrator wurde erfolgreich bearbeitet.";
																			// Message an View uebergeben
			    } else {												// Sonst
			    	table::insert("login",$_POST["login"]);					// Login Speichern
					table::insert("password",md5($_POST["pw"]));			// Passwort Speichern
					table::insert("rechte",implode(":",$_POST["rechte"]));	// Rechte Speichern
					table::insertInto();									// Einfuegen
					
					view::$data["message"] 	= "Administrator wurde erfolgreich gespeichert.";
																			// Message an View uebergeben
			    }														// Ende
		    } else {
		    	view::$data["message"] 	= "Es wurden keine Daten zum speichern übermittelt!";
																		// Message an View uebergeben
		    }
		} else {
			view::$data["message"] 	= "Dieser Administrator kann nicht bearbeitet werden!";
																		// Message an View uebergeben
		}
		view::$data["seitentitel"] 	= "Administrator speichern.";	// Seitentitel an View uebergeben
		view::$data["redirect"] 	= func::writeAdminURL("modul=admin");	
																	// Redirect an View uebergeben
		view::$data["content"] 		= "views/system/message.phtml";
																	// Template an View uebergeben
        view::render();												// Template rendern
    }
    
	public function entfernen() 
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
    	   															// SEO-URL auslesen
		if( isset($script["id"]) && $script["id"] !="" ) {			// ID abfragen
			$id = $script["id"];						
		} else {
			$id = 0;
		}
		if( $id != 0 ) {											// Abfragen ob ID nicht 0
			
			table::reset();												// vorherige DB-Abfrage leeren
	    	table::$tablename = "eat_administratoren";       				// Verbindung mit Tabelle 'administratoren' herstellen
	    	table::set_where("id","=",$id);								// ID abfragen
	    	table::set_where(" AND superadmin","=","1");				// Superadmin abfragen
	    	table::get_array();
	    	
	    	$anz = count(table::$data);
		} else {
			$anz = 0;
		}
		if( $anz == 0 ) {
		    table::reset();												// vorherige DB-Abfrage leeren
		    table::$tablename = "eat_administratoren";       				// Verbindung mit Tabelle 'news' herstellen
		    
		    $script = func::readAdminURL($this->scriptPath,parent::$seoURL);
	    	   															// SEO-URL auslesen
	   		if(isset($script["id"]) && (!empty($script["id"]) OR $script["id"]!= 0)){
	   																	// Wenn ID gesetzt
				table::set_where("id","=",$script["id"]);					// ID abfragen
				
				table::delete();											// Administrator entfernen
				view::$data["message"] 	= "Administrator erfolgreich entfernt.";		
																			// Message an View uebergeben
		    } else {													// Sonst
		    	view::$data["message"] 	= "Administrator konnte nicht entfernt werden.";
		    																// Message an View uebergeben
		    }															// Ende
			view::$data["seitentitel"] 	= "Administrator entfernen";	// Seitentitel an View uebergeben
			view::$data["redirect"] 	= func::writeAdminURL("modul=admin");
																		// Redirect an View uebergeben
		} else {													// Sonst
			view::$data["message"] 	= "Dieser Administrator kann nicht entfernt werden!";
																		// Message an View uebergeben
			view::$data["seitentitel"] 	= "Administrator entfernen";	// Seitentitel an View uebergeben
		}															// Ende
		view::$data["content"] 		= "views/system/message.phtml";	
																	// Template an View uebergeben
        view::render();												// Template rendern
    }
    
   	private function eintragen() 
   	{
   		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
    	   															// SEO-URL auslesen
		if( isset($script["id"]) && $script["id"] !="" ) {
			$id = $script["id"];
		} else {
			$id = 0;
		}
		if( $id != 0 ) {											// Abfragen ob ID nicht 0
			table::reset();												// vorherige DB-Abfrage leeren
	    	table::$tablename = "eat_administratoren";       				// Verbindung mit Tabelle 'administratoren' herstellen
	    	table::set_where("id","=",$id);								// ID abfragen
	    	table::set_where(" AND superadmin","=","1");				// Superadmin abfragen
	    	table::get_array();
	    	
	    	$anz = count(table::$data);
		} else {
			$anz = 0;
		}
		if( $anz == 0 ) {
	   		if(isset($script["id"])) {									// Wenn ID gesetzt
		        table::reset();												// vorherige DB-Abfrage leeren
		        table::$tablename = "eat_administratoren";       							// Verbindung mit Tabelle 'news' herstellen
				table::set_where("id","=",$script["id"]);					// ID Abfragen
		        table::get_array();											// Daten aus Datenbank in Array speichern
				
		        $array = array();											// Leeres Array erstellen
		
		        if(!empty(table::$data)) {									// Abfragen ob Datenbank werte enthaellt
		        	view::$data["id"]   = $script["id"];						// ID an View uebergeben
			        view::$data["data"] = table::$data[0];						// Array an View uebergeben
		        } else {													// wenn keine Werte vorhanden sind
		        	view::$data["data"] = false;								// leeren Wert an View uebergeben
		        }															// Ende
		        view::$data["seitentitel"] = "Administrator bearbeiten";				// Seitentitel an View uebergeben
	   		} else {													// Sonst
	   			view::$data["seitentitel"] = "Administrator eintragen";				// Seitentitel an View uebergeben
	   			view::$data["data"] = false;								// nix an View uebergeben
	   		}  															// Ende

	   		
	   		$error = array();											// Leeres Array fuer Fehlermeldungen
	   		if( count($_POST) > 0 ) {									// wenn Post gesetzt ist
				if( $_POST["login"] == "" ) {								// wenn id leer ist
					$error["login"] = "<div class='red bold ml110'>Bitte einen Loginname eingeben!</div>";
				}															//
	   			if( $_POST["pw"] == "" ) {								// wenn mail leer ist
					$error["pw"] = "<div class='red bold ml110'>Bitte ein Passwort eingeben!</div>";
				}															//
				
				if( count($error) == 0 ) {									// wenn keine Fehlermeldungen gesetzt
					$this->speichern();											// Speichern-Methode aufrufen
					return;														// zur Methode springen
				}															// ende
	   		}															// ende

	   		view::$data["error"]    = $error;							// Fehler an View uebergeben
	        view::$data["content"] 	= "views/admin/eintragen.phtml";	// Template an View uebergeben
		} else {
			view::$data["message"] 	= "Dieser Administrator kann nicht bearbeitet werden!";
																		// Message an View uebergeben
			view::$data["seitentitel"] 	= "Administrator bearbeiten";	// Seitentitel an View uebergeben
			view::$data["content"] 		= "views/system/message.phtml";	
																	// Template an View uebergeben
		}
        view::render();												// Template rendern
   	}
  	
  	
   	/**
   	 * Alle Eintraege anzeigen
   	 */
    private function alle() 
    {
    	$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesem
	
         table::reset();																// vorherige DB-Abfrage loeschen
        table::$tablename = "eat_administratoren";       										// Verbindung mit Tabelle 'benutzer' herstellen
		table::order_by("login");													// Sortierung nach name
		
		table::get_array();															// Daten aus Datenbank in Array speichern
		
        $array = array();															// Leeres Array erstellen

        if(!empty(table::$data)) {													// Abfragen ob Datenbank werte enthaellt
	        foreach(table::$data as $arr) {												// Array mit Foreach-Schleife durchlaufen	
        		$array[] = array(															// Array mit Werten fuellen
        			"id"		=> $arr["id"],													// ID einfuegen
        			"name"		=> $arr["login"],												// Name einfuegen
        			"superadmin"=> $arr["superadmin"]											// Superadmin einfuegen
        		);																			// Fertig
	        }																			// Schleifenende
        	view::$data["data"] = $array;												// Neues Array an View uebergeben
        } else {																	// wenn keine Werte vorhanden sind
        	view::$data["data"] = false;												// leeren Wert an View uebergeben
        }																			// Abfrage ende
    	        
        view::$data["seitentitel"] 	= "Alle Administratoren";						// Titel an View uebergeben
        view::$data["content"] 		= "views/admin/liste.phtml";					// Template an View uebergeben
        view::render();																// Template rendern
    }
}
?>