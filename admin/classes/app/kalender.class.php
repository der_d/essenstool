<?php

class kalender extends Application
{
	public function __construct($script_path,$seoURL,$dirPath) 
	{  		
    	Application::__construct($script_path,$seoURL,$dirPath);
  	}
	
	public function geheZu() 
	{
  		if($this->userdata['rights'] == 1 ) {										// Abfragen, ob eingeloggt
	  		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesen
		  	$script = isset($script["aktion"]) ? $script["aktion"] : "kalender";		// Abfragen, ob aktion gesetzt
		  	$this->$script();															// Methodenaufruf
	  	} else {																	// Sonst
  			parent::verboten();															// Error-Methode aufrufen
  		}																			// Sonst
  	}
  	
  	public function kalender() 
  	{
		$startDay  = strtotime(date('Y-\\W'.parent::$kw));
		$endDay    = strtotime("+5 day",$startDay);
		$dateArray = array();

		$data = kalenderModell::data($startDay,$endDay);
		
		$status = kalenderModell::status($startDay,$endDay);

		$bestellArray = array();
		foreach( $data as $var ) {
			
			$bestellArray[$var['date']][$var['typ']] = array(
				'id'	=> $var['id'],
				'name'	=> $var['name'],
				'kcal'	=> $var['kcal'],
				'date'	=> $var['date'],
				'price'	=> $var['price'],
				'typ'	=> $var['typ']
			);
		}
		
		// neues Array fuer Kalendereintraege erstellen
		$newArray = array();
		
		if( count($bestellArray) > 0 ) {
			for( $i=0; $i<=4; $i++ ) {
				$date = strtotime("+$i day",$startDay);
	
				for( $x=1; $x<=8; $x++ ) {
					@$newArray[$date][$x] = array(
						'id'	=> $bestellArray[$date][$x]['id'],
						'name'	=> $bestellArray[$date][$x]['name'],
						'kcal'	=> $bestellArray[$date][$x]['kcal'],
						'price'	=> $bestellArray[$date][$x]['price'],
					);
				}
			}
		} else {
			if( file_exists($_SERVER["DOCUMENT_ROOT"]."/".func::$dirPath."/admin/themes/data/kw".parent::$kw.".xml") ) {
				$xml = simplexml_load_file($_SERVER["DOCUMENT_ROOT"]."/".func::$dirPath."/admin/themes/data/kw".parent::$kw.".xml");
				
				unset($xml->day[5]);
				
				$i = 0;
				foreach($xml as $key => $value) {
					$dayNumber = $value->attributes();
					$number = $dayNumber->nr;
					$number -= 1;
					$date = strtotime("+$number day",$startDay);
					
					$x = 1;
					foreach($value->menu as $key2 => $value2) {
						$be = $value2->be != "" ? str_replace(' ','',rtrim($value2->be))." BE/Portion" : "";
						$descr = $value2->description != "" ? rtrim(ltrim($value2->description)) : "";
						
						@$newArray[$date][$x] = array(
							'id'	=> "",
							'name'	=> rtrim(ltrim($value2->name)).' '.$descr.str_replace('\n','',$be),
							'kcal'	=> str_replace(' ','',$value2->power),
							'price'	=> str_replace(' ','',number_format($value2->price,2)),
						);
						$x++;
					}
					
					$i++;
				}
			} else {
				for( $i=0; $i<=4; $i++ ) {
					$date = strtotime("+$i day",$startDay);
		
					for( $x=1; $x<=8; $x++ ) {
						$newArray[$date][$x] = array(
								'id'	=> "",
								'name'	=> "",
								'kcal'	=> "",
								'price'	=> "",
							);
					}
				}
			}
		}
	
		
		
		view::$data['data'] = $newArray;
		view::$data['kw']	= parent::$kw;
  		
		view::$data['seitentitel'] 	= 'Kalender - Woche '.parent::$kw;
		view::$data['content'] 		= 'views/kalender/index.phtml';
		view::render();
	}

	public function status()
	{
		$date = strtotime(date('Y-\\W'.self::$kw));
		
		$startDate = date('W',strtotime("-7 day",$date));
		$endDate   = date('W',strtotime("+7 day",$date));

		
			
		view::$data['data'] 	   = kalenderModell::kwStatus($startDate,$endDate);
		view::$data['seitentitel'] = 'Status Kalenderwochen';
		view::$data['content']     = 'views/kalender/status.phtml';
		
		view::render();
	}

	public function speichern()
	{
		$data = $_POST['data'];
		
		
		$startDay  = strtotime(date('Y-\\W'.$_POST['kw']));
		$endDay    = strtotime("+7 day",$startDay);
		
		foreach( $data as $key => $arr ) {

			foreach($arr as $var) {
                if( isset($var['name']) && $var['name'] != '' && isset($var['price']) && $var['price'] != '' ) {

                    $price = str_replace(',','.',$var['price']);

                    table::reset();
                    table::$tablename = 'eat_menu';

                    table::insert('name',$var['name']);
                    table::insert('kcal',$var['kcal']);
                    table::insert('price',$price);
                    table::insert('typ',$var['typ']);
                    table::insert('date',$key);

                    if( isset($var['id']) && $var['id'] != '' ) {
                        table::set_where('id','=','"'.$var['id'].'"');
                        table::update();
                    } else {
                        table::insertInto();
                    }
                } else {
                    if( isset($var['id']) && $var['id']!='' ) {
                        table::reset();
                        table::$tablename = 'eat_menu';
                        table::set_where('id','=',"'".$var['id']."'");
                        table::delete();
                    }
                }
			}
		}
		
		if( kalenderModell::mailed($_POST['kw']) !== false ) {
			table::reset();
			table::$tablename = 'eat_benutzer';
			table::get_data();
			
			foreach( table::$data as $var ) {
		        
		        $empfaenger = $var['email'];
		        
		        $betreff    = "Essenstool KW ".$_POST['kw'];
		        
		        $header  = "From: Essenstool <danny.curth@unister.de>\n";
		        $header .= "MIME-Version: 1.0\n";
	        	$header .= "Content-type: text/html; charset=UTF-8\n";
	        	$header .= "Content-Transfer-Encoding: 8BIT\r\n";
		        
		        $nachricht  = "
		        <html>
				<head>
				    <title>$betreff</title>
				</head>
				<body>
					Hallo ".$var['name'].".<br />
					Es wurde ein neuer Eintrag im Essenstool für die Kalenderwoche ".$_POST['kw']." gemacht.<br />
					Logge dich am besten gleich ein und bestell dein Essen.
					<br /><br />
					<a href='".func::$scriptPath."'>".func::$scriptPath."</a>
		        </body></html>";
		        
		        mail($empfaenger, $betreff, $nachricht,$header);
			}
			
			table::reset();
			table::$tablename = 'eat_datum';
			table::set_where('kw','=','"'.$_POST['kw'].'"');
			table::insert('mail','1');
			table::update();
		}
		
		$status = kalenderModell::dateStatus($_POST['kw']);
		if( $status === false ) {
			table::reset();
			table::$tablename = 'eat_datum';
					
			table::insert('kw',$_POST['kw']);
			table::insert('open',1);
			table::insertInto();
		}
		
		view::$data['seitentitel'] 	= 'Essensplan erfolgreich gespeichert';
		view::$data['message']		= 'Der Essensplan für diese KW wurde erfolgreich gespeichert';
		view::$data['redirect']		= func::writeAdminURL('modul=kalender,kw='.$_POST['kw']);
		view::$data['content']		= 'views/system/message.phtml';
		view::render();
	}

	public function close()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		$kw = $script['kw'];
		
		kalenderModell::setStatus($kw,0);
		
		view::$data['seitentitel']	= 'Bestellm&ouml;glichkeit abgeschlossen';
		view::$data['message']		= 'Die M&ouml;glichkeit, in dieser KW etwas zu bestellen wurde abgeschlossen.';
		view::$data['redirect']		= func::writeAdminURL('modul=kalender,aktion=status');
		view::$data['content']		= 'views/system/message.phtml';
		view::render();
	}
	
	public function open()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		$kw = $script['kw'];
		
		kalenderModell::setStatus($kw,1);
		
		view::$data['seitentitel']	= 'Bestellm&ouml;glichkeit geöffnet';
		view::$data['message']		= 'Die M&ouml;glichkeit, in dieser KW etwas zu bestellen wurde gewährt.';
		view::$data['redirect']		= func::writeAdminURL('modul=kalender,aktion=status');
		view::$data['content']		= 'views/system/message.phtml';
		view::render();
	}
}