<?php 
class user extends Application
{
	public function __construct($script_path,$seoURL,$dirPath) 
	{  		
    	Application::__construct($script_path,$seoURL,$dirPath);
  	}
	
	public function geheZu() 
	{
  		if($this->userdata['rights'] == 1 ) {										// Abfragen, ob eingeloggt
	  		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesen
		  	$script = isset($script["aktion"]) ? $script["aktion"] : "user";		// Abfragen, ob aktion gesetzt
		  	$this->$script();															// Methodenaufruf
	  	} else {																	// Sonst
  			parent::verboten();															// Error-Methode aufrufen
  		}																			// Sonst
  	}
  	
  	public function user() 
  	{
  		view::$data['data']			= userModell::users();
		
		view::$data['seitentitel'] 	= 'Benutzer';
		view::$data['content'] 		= 'views/user/index.phtml';
		view::render();
	}

	public function bearbeiten()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		$userID = $script['user'];
		
		
		view::$data['data'] 	   = userModell::userData($userID);
		
		
		view::$data['seitentitel'] 	= 'Benutzer bearbeiten';
		view::$data['content']     	= 'views/user/edit.phtml';
		
		view::render();
	}

	public function speichern()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		$userID = $script['user'];
		
		
		userModell::save($userID);
		
		
		view::$data['seitentitel'] 	= 'User bearbeitet';
		view::$data['message']		= 'Du hast erfolgreich diesen Benutzer bearbeitet.';
		view::$data['redirect']		= func::writeAdminURL('modul=user,user='.$userID);
		view::$data['content']		= 'views/system/message.phtml';
		
		view::render();
	}

	public function entfernen()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);
		$userID = $script['user'];
		
		
		userModell::delete($userID);
		
		
		view::$data['seitentitel']	= 'Benutzer erfolgreich entfernt';
		view::$data['message']		= 'Du hast den Benutzer erfolgreich gelöscht.';
		view::$data['redirect']		= func::writeAdminURL('modul=user');
		view::$data['content']		= 'views/system/message.phtml';
		
		view::render();
	}
}