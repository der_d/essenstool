<?php 
class bestellung extends Application
{
	public function __construct($script_path,$seoURL,$dirPath) 
	{  		
    	Application::__construct($script_path,$seoURL,$dirPath);
  	}
	
	public function geheZu() 
	{
  		if($this->userdata['rights'] == 1 ) {										// Abfragen, ob eingeloggt
	  		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesen
		  	$script = isset($script['aktion']) ? $script['aktion'] : "bestellung";		// Abfragen, ob aktion gesetzt
			$this->$script();															// Methodenaufruf
	  	} else {																	// Sonst
  			parent::verboten();															// Error-Methode aufrufen
  		}																			// Sonst
  	}
  	
  	public function bestellung() 
  	{
		$startDay  = strtotime(date('Y-\\W'.parent::$kw));
		$endDay    = strtotime("+5 day",$startDay);
		$dateArray = array();

		$data = bestellungModell::data($startDay,$endDay);
	
		$bestellung = bestellungModell::bestellt($startDay,$endDay);
		
		$status = bestellungModell::status($startDay,$endDay);
		
		$user = bestellungModell::user();

		$bestellArray = array();
		foreach( $bestellung as $var ) {
			
			$bestellArray[$var['date']] = array(
				'id'	=> $var['id'],
				'user'	=> $var['user'],
				'date'	=> $var['date'],
				'typ'	=> $var['typ']
			);
		}
	
		$knuff = array();
		for( $i=0; $i<=4; $i++ ) {
			$date = strtotime("+$i day",$startDay);
			
			for( $x=1; $x<=8; $x++ ) {
				@$knuff[$date]['m'.$x] = array(
					'id'	=> $bestellArray[$date]['id']
				);
			}
		}
		
		foreach($user as $var) {
			$dat = bestellungModell::bestellungen($var['id'],$startDay,$endDay);
			view::$data['users'][] = array(
				'id'	=> $var['id'],
				'name'	=> $var['name'],
				'data'	=> $dat
			);
		}
	
		if( count($data) > 0 ) {
			foreach( $data as $var ) {
				view::$data['data'][$var['date']]['m'.$var['typ']] = array(
					'id'	=> $var['id'],
					'name'	=> $var['name'],
					'kcal'	=> $var['kcal'],
					'price'	=> $var['price']
				);
			}
		} else {
			view::$data['data'] = false;
		}
		
		view::$data['aktion'] = '';
  		view::$data['seitentitel'] = 'Bestellungen KW:'.parent::$kw;
		view::$data['content'] = 'views/bestellung/index.phtml';
		view::render();
	}

	public function uebersicht()
	{
		$startDay  = strtotime(date('Y-\\W'.parent::$kw));
		$endDay    = strtotime("+5 day",$startDay);
		
		$data = bestellungModell::orders($startDay,$endDay);
	
		if( count($data) > 0 ) {
			view::$data['data'] = $data;
		} else {
			view::$data['data'] = false;
		}
		
		view::$data['aktion'] = 'uebersicht';
		view::$data['seitentitel'] = 'Bestell&uuml;bersicht - Bestellungen - KW:'.parent::$kw;
		view::$data['content'] = 'views/bestellung/list.phtml';
		view::render();
	}
	
	public function benutzer()
	{
		$startDay  = strtotime(date('Y-\\W'.parent::$kw));
		$endDay    = strtotime("+5 day",$startDay);
		
		$data = bestellungModell::userOrders($startDay,$endDay);
		
		if( count($data) > 0 ) {
			view::$data['data'] = $data;
		} else {
			view::$data['data'] = false;
		}

		
		$status = bestellungModell::userStatus(parent::$kw);
	
		if( count($status) > 0 ) {
			view::$data['payed'] = $status;
		} else {
			view::$data['payed'] = false;
		}
		
		
		view::$data['aktion'] = 'benutzer';
		view::$data['seitentitel'] = 'Bestell&uuml;bersicht - Benutzer - KW:'.parent::$kw;
		view::$data['content'] = 'views/bestellung/users.phtml';
		view::render();
	}
	
	public function payed()
	{
		$script = func::readAdminURL($this->scriptPath,parent::$seoURL);			// SEO-URL auslesen
		
		if( bestellungModell::payStatus($script['kw'],$script['user']) ) {
			bestellungModell::setPayed($script['kw'],$script['user'],0);
			
			view::$data['seitentitel'] 	= 'nicht Bezahlt gesetzt';
			view::$data['message']		= 'User erfolgreich auf nicht bezahlt gesetzt';
		} else {
			bestellungModell::setPayed($script['kw'],$script['user'],1);
			
			view::$data['seitentitel'] 	= 'Bezahlt gesetzt';
			view::$data['message']		= 'User erfolgreich auf bezahlt gesetzt';
		}
		view::$data['redirect']		= func::writeAdminURL('modul=bestellung,aktion=benutzer,kw='.$script['kw']);
		view::$data['content'] 		= 'views/system/message.phtml';
		view::render();
	}
	
	public function saveOrder()
	{
		$kw = $_POST['kw'];
		
		$startDay  = strtotime(date('Y-\\W'.$kw));
		$endDay    = strtotime("+5 day",$startDay);
		
		bestellungModell::deleteKwOrders($startDay,$endDay);
		
		$data = $_POST['value'];
		
		if( count($data) > 0 ) {
					
			foreach($data as $var) {
				
				$value = explode('#',$var);
			
				bestellungModell::insertSingleOrder(
					$value[0], // Datum
					$value[1], // User
					$value[3]  // Menue-ID
				);
			}
			
		}
		
		view::$data['seitentitel'] 	= 'Bestellungen aktualisiert';
		view::$data['message']		= 'Die Bestellungen wurden aktualisiert';
		view::$data['redirect']		= func::writeAdminURL('modul=bestellung,kw='.$kw);
		view::$data['content'] 		= 'views/system/message.phtml';
		view::render();
	}

}