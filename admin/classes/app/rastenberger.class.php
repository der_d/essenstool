<?php
//If use in the console:
//$arguments = $argv;
//array_shift($arguments);
//$kw = $arguments[0];
$kw = strtotime($_GET['year'] . 'W' . str_pad($_GET['kw'], 2, '0', STR_PAD_LEFT));

/**
 * The Meal object
 *
 * @author Marcel Steinmann <marcel.steinmann@unister.de>
 * @version 1.0
 * @since 2014-03-10 14:50
 */
class Meal
{
    /**
     * @var string
     */
    protected $name = '';
    /**
     * @var float
     */
    protected $price = 0.0;
    /**
     * @var int
     */
    protected $kcal = 0;
    /**
     * @var DateTime
     */
    protected $date = null;
    /**
     * @var int
     */
    protected $be = 0;
    /**
     * @var int
     */
    protected $type = 0;

    public function __construct()
    {
        $this->setDate(new DateTime());
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $kcal
     * @return $this
     */
    public function setKcal($kcal)
    {
        $this->kcal = $kcal;
        return $this;
    }

    /**
     * @return int
     */
    public function getKcal()
    {
        return $this->kcal;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $be
     * @return $this
     */
    public function setBe($be)
    {
        $this->be = $be;
        return $this;
    }

    /**
     * @return int
     */
    public function getBe()
    {
        return $this->be;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }
}

/**
 * Repository for the meals
 *
 * @author Marcel Steinmann <marcel.steinmann@unister.de>
 * @version 1.0
 * @since 2014-03-11 08:50
 */
class MealRepository
{
    /**
     * @var PDO
     */
    protected $sqlConnection = null;

    /**
     * initialize the object
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * connect on the db
     */
    protected function connect()
    {
        if (is_null($this->sqlConnection)) {
            $this->sqlConnection = new PDO('mysql:dbname=essenstool;host=localhost', 'essenstool', 'essenstool');
        }
    }

    /**
     * save all meals
     * @param ArrayObject $meals
     */
    public function saveMeals(ArrayObject $meals)
    {
        /* @var $meal Meal */
        $inserts = array();
        foreach ($meals as $meal) {
            $inserts[] = $meal->getDate()->getTimestamp() . ',' . floatval($meal->getKcal()) . ','
                . number_format(floatval($meal->getPrice()), 2) . ',' . intval($meal->getType()) . ','
                . $this->sqlConnection->quote($meal->getName());
        }
        $this->sqlConnection->query(
            'INSERT INTO eat_menu (`date`, `kcal`, `price`, `typ`, `name`) VALUES (' . implode('),(', $inserts) . ')'
        );
    }
}

/**
 * Rastenberger methods
 *
 * @author Marcel Steinmann <marcel.steinmann@unister.de>
 * @version 1.0
 * @since 2014-03-10 12:50
 */
class rastenberger
{
    /**
     * @var int
     */
    protected $kw = 0;

    /**
     * @var MealRepository
     */
    protected $repository = null;

    /**
     * initialize the object and the repository
     */
    public function __construct()
    {
        $this->repository = new MealRepository();
    }

    /**
     * @param string $output
     */
    protected function write($output)
    {
//        echo $output;
    }

    /**
     * @param int $kw
     */
    public function start($kw)
    {
        $this->kw = $kw;
        $this->write('Begin KW ' . date('W/Y', $kw) . PHP_EOL);
        $kwMeals = $this->request();
        $this->write('Read the response.' . PHP_EOL);
        $mealsToSave = $this->matchAllMeals($kwMeals);
        $this->write('Save the meals.' . PHP_EOL);
        $this->repository->saveMeals($mealsToSave);
        $this->write('It was saved ' . $mealsToSave->count() . ' meals.' . PHP_EOL);
		if (!isset($_SERVER['HTTP_REFERER']) || empty($_SERVER['HTTP_REFERER'])) {
			$redirect = $_SERVER['HTTP_HOST'];
		} else {
			$redirect = $_SERVER['HTTP_REFERER'];
		}
		header('Location: ' . $redirect);
    }

    /**
     * @param string $kwMeals
     * @return ArrayObject
     */
    protected function matchAllMeals($kwMeals)
    {
        $holidayPattern = '[^(menuCont")]*?(Feiertag|Maifeiertag|Silvester|Neujahr|Oster.*)';
        $zuckerPattern = '<.+?>(\d,\d) BE/Portion<.+?>';
        $defaultPattern = '<a.+?>';
        $nachtischPattern = '.+?>';
        $allMeals = new ArrayObject();

        $regex =
            '#menuCont".+?z-index:([\ \d]+);.*?class="menuLong">(' . $holidayPattern
            . '|(<span style="[\w\d :\-;]+?">(.+?)</span>( (.+?)([\d\.]+) kcal/Portion(' . $defaultPattern
            . '|' . $zuckerPattern . ')|' . $nachtischPattern . ')(Preis: (\d,\d{2})|' . $holidayPattern . ')))#';
        preg_match_all(
            $regex,
            $kwMeals,
            $findMeals
        );

        /**
        old pattern (python)
        patternHoliday = re.compile('class="menuLong"><.+><div class="menuShort clearfix">(Feiertag|Silvester|Neujahr|Oster.*)<')
        patternZucker = re.compile(u'class="menuLong"><span style="[\w\d :\-;]+">(.+)</span> (.+)<br/>\d{3}.+>(\d{3}) kcal/Portion<.+>(\d,\d) BE/Portion<.+>Preis: (\d,\d{2}) €<')
        patternDefault = re.compile(u'class="menuLong"><span style="[\w\d :\-;]+">(.+)</span> (.+)<br/>(\d+) kcal/Portion<a.+>Preis: (\d,\d{2}) €')
        patternNachtisch = re.compile(u'class="menuLong"><span style="[\w\d :\-;]+">(.+)</span>.+>Preis: (\d,\d{2}) €<')
         */

        /**
         * old:
         * Indexes:
         * 0 => all HTML -> IGNORE
         * 1 => days (6 = monday, 1 = weekend)
         * 2 => menu-description HTML -> IGNORE
         * 3 => holiday
         * 4 => meal-name
         * 5 => meal-additional HTML -> IGNORE
         * 6 => meal-name-additional
         * 7 => kcal
         * 8 => BE HTML -> IGNORE
         * 9 => BE/Portion
         * 10 => Price
         */

        /**
         * new:
         * Indexes:
         * 0 => all HTML -> IGNORE
         * 1 => days (6 = monday, 1 = weekend)
         * 2 => menu-description HTML -> IGNORE
         * 3 => menuCont
         * 4 => holiday
         * 5 => meal-name
         * 6 => meal-additional HTML -> IGNORE
         * 7 => meal-name-additional
         * 8 => kcal
         * 9 => BE HTML -> IGNORE
         * 10 => BE/Portion
         * 11 => Price oder Holday
         * 12 => Price
         * 13 => Holiday
         */

        $mealType = array_fill(1, 7, 0);
        foreach ($findMeals[1] as $offset => $day) {
            $day = trim($day);
            if ($day === '1') {
                continue;
            }
            $meal = new Meal();
            $findMeals[12][$offset] = floatval(str_replace(',', '.', $findMeals[12][$offset]));
            $findMeals[9][$offset] = floatval(str_replace(',', '.', $findMeals[9][$offset]));
            $findMeals[8][$offset] = floatval(str_replace(array('.', ','), array('', '.'), $findMeals[8][$offset]));
            if (!empty($findMeals[13][$offset])) {
                $findMeals[4][$offset] = $findMeals[13][$offset];
                $findMeals[7][$offset] = '';
            }
            $meal->setName(html_entity_decode(strip_tags($findMeals[5][$offset] . $findMeals[7][$offset])))
                ->setType(++$mealType[$day])
                ->setBe($findMeals[10][$offset])
                ->setPrice($findMeals[12][$offset])
                ->setKcal($findMeals[8][$offset])
                ->getDate()->setTimestamp($this->kw)
                ->add(new DateInterval('P' . (6 - $day) . 'D'));
            $allMeals->append($meal);
        }
        return $allMeals;
    }

    /**
     * @param int $kw
     * @return string
     */
    protected function request()
    {
        $postdata = http_build_query(
            array(
                'id_s' => '14',
                'ln' => 'de',
                'wk' => '',
                'sent' => '',
                'kw' => $this->kw,
                'proDay' => 0
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents('http://rastenberger.de/index.php', false, $context);
        return $result;
    }
}

$t = new rastenberger();
$t->start($kw);
