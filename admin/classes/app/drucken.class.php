<?php 
class drucken extends Application
{
	/**
	 * Konstruktor, der den Konstruktor seiner Elternklasse aufruft
	 * wichtig, da in dem Konstruktor der Elternklasse entsprechende Werte zu finden sind
	 * 
	 * @param string $script_path
	 * @param string $seoURL
	 * @param string $mail
	 * @param string $dirPath
	 */
	public function __construct($script_path,$seoURL,$mail,$dirPath) 
	{  		
		$this->dirPath	= $dirPath;
		$this->mail		= $mail;
  		$this->path 	= $script_path;
    	Application::__construct($script_path,$seoURL,$mail,$dirPath);
  	}

	public function geheZu() 
	{
  		if(func::adminLogged()) {										// Abfragen, ob eingeloggt
	  		if(func::adminRight(func::adminLogged(),"or")) {				// Rechte abfragen
	  			$script = func::readAdminURL($this->scriptPath,parent::$seoURL);// SEO-URL auslesen
		  		$script = isset($script["aktion"]) ? $script["aktion"] : "alle";		// Abfragen, ob aktion gesetzt
		  		$this->$script();												// Methodenaufruf
	  		} else {														// Sonst
	  			parent::access();												// Zugriff verweigert - Methode aufrufen
	  		}																// Ende
	  	} else {														// Sonst
  			parent::verboten();												// Error-Methode aufrufen
  		}																// Sonst
  	}
  	
	/**
   	 * Daten aus Datenbank auslesen und ausgeben
   	 */
	private function bestellungdrucken() 
	{
    	$script = func::readAdminURL($this->scriptPath,parent::$seoURL); 
    															// SEO URL auslesen
    	table::reset();											// vorherige DB-Abfrage leeren
        table::$tablename = "eat_bestellung_position";				// Verbindung mit Tabelle 'statisch' herstellen
        table::set_where("id_bestellung","=",$script["id"]);	// ID abfragen
        table::get_array();										// Daten in Array speichern
    	
        $array = array();										// Leeres Array erstellen
        $artikel = array();
        foreach(table::$data as $arr) {							// Bestellungspositionen mit Foreach-Schleife durchlaufen
        	if( $arr["typ"] == 4 ) {
        		$artikel[] = $arr;	
        	} else {
        		$array[] = $arr;										// Werte in neues Array speichern
        	}
        }														// Ende
        	
        table::reset();											// vorherige DB-Abfrage leeren
        table::$tablename = "eat_bestellung";       					// Verbindung mit Tabelle 'statisch' herstellen
        table::set_where("id","=",$script["id"]);				// ID abfragen
        table::get_array();										// Daten in Array speichern
		
        if(!empty(table::$data)) {								// Abfragen ob Array werte enthaellt
	        view::$data["data"] = table::$data[0];					// Array an View uebergeben
	        view::$data["data"]["inhalt"] = $array;					// Filme/Spiele - Positionen fuer Bestellung
	        view::$data["data"]["artikel"] = $artikel;				// Artikel - Positionen fuer Bestellung	
	        view::$data["id"] =	$script["id"];		
        } else {												// Sonst
        	view::$data["data"] = false;							// leeren Wert an View uebergeben
        }														// Ende
        view::$template	= "views/drucken/layout.phtml";				// Template
        view::$data["seitentitel"] = "Bestellung #".table::$data[0]["id"];
        														// Seitentitel an View uebergeben
        view::$data["content"] = "views/drucken/bestellung.phtml";	
        														// Content-Template an View uebergeben
        view::render();											// Template rendern
    }
    
	/**
   	 * Daten aus Datenbank auslesen und ausgeben
   	 */
	private function rechnungdrucken() 
	{
    	$script = func::readAdminURL($this->scriptPath,parent::$seoURL); 
    															// SEO URL auslesen
    	table::reset();											// vorherige DB-Abfrage leeren
        table::$tablename = "eat_bestellung_position";				// Verbindung mit Tabelle 'statisch' herstellen
        table::set_where("id_bestellung","=",$script["id"]);	// ID abfragen
        table::set_where(" AND typ","=","'4'");					// ist Artikel abfragen
		table::set_where(" AND status","!=","6");				// ist Artikel noch verfuegbar
        table::get_array();										// Daten in Array speichern
    	
        $array = array();										// Leeres Array erstellen
        $artikel = array();
        foreach(table::$data as $arr) {							// Bestellungspositionen mit Foreach-Schleife durchlaufen
        	if( $arr["typ"] == 4 ) {
        		$artikel[] = $arr;	
        	} else {
        		$array[] = $arr;										// Werte in neues Array speichern
        	}
        }														// Ende
        	
        table::reset();											// vorherige DB-Abfrage leeren
        table::$tablename = "eat_bestellung";       					// Verbindung mit Tabelle 'statisch' herstellen
        table::set_where("id","=",$script["id"]);				// ID abfragen
        table::get_array();										// Daten in Array speichern
		
        if(!empty(table::$data)) {								// Abfragen ob Array werte enthaellt
	        view::$data["data"] = table::$data[0];					// Array an View uebergeben
	        view::$data["data"]["inhalt"] = $array;					// Filme/Spiele - Positionen fuer Bestellung
	        view::$data["data"]["artikel"] = $artikel;				// Artikel - Positionen fuer Bestellung	
	        view::$data["id"] =	$script["id"];		
        } else {												// Sonst
        	view::$data["data"] = false;							// leeren Wert an View uebergeben
        }														// Ende
        view::$template	= "views/drucken/layout.phtml";				// Template
        view::$data["seitentitel"] = "Bestellung #".table::$data[0]["id"];
        														// Seitentitel an View uebergeben
        view::$data["content"] = "views/drucken/rechnung.phtml";	
        														// Content-Template an View uebergeben
        view::render();											// Template rendern
    }
}
?>